MinePath
================
A pathway analysis methodology aiming to facilitate and ease the identification of differentially functional paths or 
sub-paths within a gene regulatory network, using gene-expression data. The methodology takes advantage of the topology 
and the underlying regulatory mechanisms of gene regulatory net-works, including the direction and the type of the 
engaged interactions (e.g. activation/expression, inhibition). 

Licensed under the Apache License, Version 2.0

Web based implementation with visualization features can be found in www.minepath.org
