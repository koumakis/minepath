package misc;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.util.Set;
import java.util.Vector;


public class saveToFile {
	public Hashtable mergedData = new Hashtable();
	public List<String> mergedNonFunctionalSubPaths = new ArrayList<String>();
	static int DEBUGmode = 0;
	
	public saveToFile(int DebugLevel){
		DEBUGmode = DebugLevel;
	}
	public void addDataForMerge(Hashtable dataSet, List<String> nonFunctionalSubPaths){
		mergedData.putAll(dataSet);
		mergedNonFunctionalSubPaths.removeAll(nonFunctionalSubPaths);
		mergedNonFunctionalSubPaths.addAll(nonFunctionalSubPaths);
	}
	
	public void addDataForMergeWithPathwayID(Hashtable dataSet, String pathway, List<String> nonFunctionalSubPaths){
		Set set = dataSet.entrySet(); 
		Iterator iter = set.iterator(); 
		while(iter.hasNext()) { 
			Map.Entry me = (Map.Entry)iter.next(); 
			mergedData.put((String)me.getKey()+"@"+pathway , (BitSet)me.getValue());
		}

		mergedNonFunctionalSubPaths.removeAll(nonFunctionalSubPaths);
		mergedNonFunctionalSubPaths.addAll(nonFunctionalSubPaths);
	}
	
	public void mergeData(String filename, String[] classes) throws IOException{
		if(DEBUGmode == 1)
			System.out.print("Writing ranked and Discretized data file to "+filename +" ");
		BufferedWriter writer = new BufferedWriter( new FileWriter(filename) );
		writer.write("feature ID\t");
		
		//TODO: delete it just for test
		//writer.write("Ranking\t");
		
		for(int i=0; i < classes.length;i++)
			writer.write(classes[i]+"\t");
		//writer.newLine();
		
		Set set = mergedData.entrySet(); 
		// Get an iterator 
		Iterator iter = set.iterator(); 
		// Display elements 
		while(iter.hasNext()) { 
			Map.Entry me = (Map.Entry)iter.next(); 
			writer.write("\n" +(String)me.getKey() +"\t");
			BitSet b = (BitSet)me.getValue();
			if(b.isEmpty()){
				for(int i=0; i<classes.length-b.length(); i++)
					writer.write("0\t");
				continue;
			}
			for(int i=0; i<b.length(); i++){
				if(b.get(i))
					writer.write("1\t");
				else
					writer.write("0\t");
			}
			for(int i=0; i<classes.length-b.length(); i++) //At BitSet the system ignores the last falses which for us are zeros...
				writer.write("0\t");
		}
		
		for(int i=0; i<mergedNonFunctionalSubPaths.size(); i++){
			writer.write("\n" +mergedNonFunctionalSubPaths.get(i) +"\t");
			for(int j=0; j<classes.length; j++)
				writer.write("0\t");
		}
		
		writer.flush();
		writer.close();
		if(DEBUGmode == 1)
			System.out.println("... Done.");
	}
	
	
	public void hashtableToTabFile(String filename, Hashtable dataSet, List<String> nonFunctionalSubPaths, String[] classes) throws IOException{
		
		if(DEBUGmode == 1)
			System.out.print("Writing ranked and Discretized data file to "+filename +" ");
		
		
		BufferedWriter writer = new BufferedWriter( new FileWriter(filename) );
		writer.write("feature ID\t");
		
		//TODO: delete it just for test
		//writer.write("Ranking\t");
		
		for(int i=0; i < classes.length;i++)
			writer.write(classes[i]+"\t");
		//writer.newLine();
		
		Set set = dataSet.entrySet(); 
		// Get an iterator 
		Iterator iter = set.iterator(); 
		// Display elements 
		while(iter.hasNext()) { 
			Map.Entry me = (Map.Entry)iter.next(); 
			writer.write("\n" +(String)me.getKey() +"\t");
			BitSet b = (BitSet)me.getValue();
			if(b.isEmpty()){
				for(int i=0; i<classes.length-b.length(); i++)
					writer.write("0\t");
				continue;
			}
			for(int i=0; i<b.length(); i++){
				if(b.get(i))
					writer.write("1\t");
				else
					writer.write("0\t");
			}
			for(int i=0; i<classes.length-b.length(); i++) //At BitSet the system ignores the last falses which for us are zeros...
				writer.write("0\t");
		}
		
		for(int i=0; i<nonFunctionalSubPaths.size(); i++){
			writer.write("\n" +nonFunctionalSubPaths.get(i) +"\t");
			for(int j=0; j<classes.length; j++)
				writer.write("0\t");
		}
		
		writer.flush();
		writer.close();
		if(DEBUGmode == 1)
			System.out.println("... Done.");
	}
	
	public void mergeFiles(Vector<String> inputFileNames, String outputFileName) throws IOException{
		
		Hashtable existingFeatures = new Hashtable();
		BufferedReader reader;
		String line;
		String keggPathName;
		Vector<String[]> tmpData = new Vector();
		
		for(int i=0; i<inputFileNames.size(); i++){
			if(inputFileNames.get(i).indexOf("hsa") > 0 ){
				keggPathName = "@" +inputFileNames.get(i).substring(inputFileNames.get(i).indexOf("hsa"), inputFileNames.get(i).indexOf("hsa") + 8);
			}
			else
				keggPathName = "@" +inputFileNames.get(i);
			reader = new BufferedReader(new FileReader(inputFileNames.get(i)));
			while ( (line = reader.readLine()) != null) {	
				String[] row = line.split("\t");
				if( existingFeatures.containsKey((String)row[0]) ){
					existingFeatures.put(row[0], existingFeatures.get((String)row[0]) +keggPathName);
					
					continue;
				}
				tmpData.add(row);
				existingFeatures.put(row[0], row[0] +keggPathName);
			}
			reader.close();
		}
		
		BufferedWriter writer = new BufferedWriter( new FileWriter(outputFileName) );
		for(int i=0; i<tmpData.size(); i++){
			writer.write( (String) existingFeatures.get(tmpData.get(i)[0]) );
			for(int j=1; j<tmpData.get(i).length; j++){
				writer.write("\t" +tmpData.get(i)[j]);
			}
			writer.newLine();
		}
		writer.flush();
		writer.close();
	}
	
	public void tabFile2arff(String inputFileName) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(inputFileName));
		
		String line;
		int counter=0;
		line = reader.readLine();
		String[] classes = line.split("\t");

		while ( (line = reader.readLine()) != null) {	
			counter ++;
		}
		reader.close();

		if(counter == 0)
			return;
		
		//Reopen to read the data (stupid...)
		reader = new BufferedReader(new FileReader(inputFileName));
		String[][] data = new String[counter][classes.length];
		//Skip the first line. we have it at classes matrix.
		line = reader.readLine();
		counter=0;
		while ( (line = reader.readLine()) != null) {	
			data[counter++] = line.split("\t");
		}
		reader.close();
		
		BufferedWriter writer = new BufferedWriter( new FileWriter(inputFileName +".arff") );
		writer.write("@Relation	" +inputFileName.replace(" ", "_"));
		writer.newLine();
		writer.newLine();
		for(int i=0; i<data.length; i++){
			writer.write("@Attribute	" +data[i][0].replace(" ", "_") +" \t{0, 1}");
			writer.newLine();
		}
		int startDataIndex;
		if( (classes[2].compareTo("Ranking") == 0) || (classes[2].compareTo("P-Value")==0) )
			startDataIndex = 3;
		else if( (classes[1].compareTo("Ranking") == 0) || (classes[1].compareTo("P-Value")==0) )
			startDataIndex = 2;
		else
			startDataIndex = 1;
		String class1 = classes[startDataIndex];
		String class2 = new String();
		for(int i=startDataIndex; i<classes.length; i++)
			if(class1.compareTo(classes[i]) != 0){
				class2 = classes[i];
				break;
			}
		
		writer.write("@Attribute	Class	{" +class1 +", " +class2 +"}" );
		writer.newLine();
		writer.newLine();
		writer.write("@Data");
		writer.newLine();
		//Read data and write it Transposed
		for(int j=startDataIndex; j<data[0].length; j++){
			for(int i=0; i<data.length; i++){	
				writer.write(data[i][j] +",");
				if(i == data.length-1){
					writer.write(classes[j]);
					writer.newLine();
				}
			}
		}

		writer.flush();
		writer.close();
	}
	
	public void zipResults() {
		try {
			File inFolder = new File("out");
			File outFolder = new File("Out.zip");
			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
					new FileOutputStream(outFolder)));
			BufferedInputStream in = null;
			byte[] data = new byte[1000];
			String files[] = inFolder.list();
			for (int i = 0; i < files.length; i++) {
				in = new BufferedInputStream(new FileInputStream(
						inFolder.getPath() + "/" + files[i]), 1000);
				out.putNextEntry(new ZipEntry(files[i]));
				int count;
				while ((count = in.read(data, 0, 1000)) != -1) {
					out.write(data, 0, count);
				}
				out.closeEntry();
			}
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}