package misc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.SMO;
import weka.classifiers.trees.J48;
import weka.core.Debug.Random;
import weka.core.Instances;

public class validation {

	/**
	 * @param args
	 * @throws Exception 
	 */
	private String valString = new String();
	

	public String getValidationResults(){
		return valString;
	}
	
	public void useValidation(String arffFile, int algorithmId, String validationFile) throws Exception{

		
		Classifier cls;
		if(algorithmId == 1){
			cls = new J48();
			valString = "Validation of Best sub-pathways.\nAlgorithm : C4.5 Decision tree (10-fold cross validation).";
		}else if(algorithmId == 2){
			cls = new NaiveBayes();
			valString = "Validation of Best sub-pathways.\nAlgorithm : NaiveBayes (10-fold cross validation).";
		}else if(algorithmId == 3){
			cls = new SMO();
			valString = "Validation of Best sub-pathways.\nAlgorithm : Support Vector Machine (10-fold cross validation).";
		}else
			return;
		
		BufferedReader reader = new BufferedReader(new FileReader(arffFile));
		Instances data = new Instances(reader);
		reader.close();
		
		BufferedWriter writer = new BufferedWriter( new FileWriter(validationFile) );
		
		// setting class attribute
		data.setClassIndex(data.numAttributes() - 1);


		Evaluation eval = new Evaluation(data);
		Random rand = new Random(1);  // using seed = 1
		int folds = 10;
		eval.crossValidateModel(cls, data, folds, rand);
	
		valString = valString.concat("\n" + eval.toSummaryString() );
		valString = valString.concat("\n" + eval.toClassDetailsString() );
		valString = valString.concat("\n" + eval.toMatrixString() );

		writer.write(valString);
		writer.flush();
		writer.close();
		
	}
	
	
}
