package discretization;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import misc.StdStats;


/**
 * Microarray Discretization according to the methodology provided by 
 * George Potamias, Lefteris Koumakis, and Vassilis Moustakis. 
 * Gene Selection via Discretized Gene-Expression Profiles and Greedy Feature-Elimination. 
 * In Vouros G. A. and Panayiotopoulos T. (eds), 
 * Methods and Applications of Artificial Intelligence, 
 * Lecture notes in Artificial Intelligence-LNAI, vol. 3025, pp. 256-266, (2004). 
 * 
 * @author koumakis
 *
 */
public class DiscrEntropyBased extends FS{

	private double simplepPredictor[];
	private boolean ranking = false;
	private boolean discrFilter = false;
	//private boolean filter2[];
	
	public DiscrEntropyBased(int DebugMode, boolean addRanking){
		DEBUGmode = DebugMode;
		numberOfClass1 = 0;
		numberOfClass2 = 0;
		ranking = addRanking;
		
	}
	
	public DiscrEntropyBased(int DebugMode){
		DEBUGmode = DebugMode;
		numberOfClass1 = 0;
		numberOfClass2 = 0;
	}
	
	public void startMetric(String inputFileName, String outputFileName, int N, boolean discF, boolean permutation) throws IOException{
		discrFilter = discF;
		
		readData(inputFileName);
		
		if(permutation){
			ArrayList<String> lotto = new ArrayList();
			for(int i=0; i< classes.length; i++)
				lotto.add(classes[i]);
			for(int i=0; i< classes.length; i++){
				int rnd =  (int)(Math.random() *lotto.size());
				classes[i] = lotto.get(rnd);
				lotto.remove(rnd);
			}
			
			/*
			double cut = (double)numberOfClass1/(double)numberOfClass2;
			int counter1 = numberOfClass1;
			int counter2 = numberOfClass2;
			String ClassName1 = Class1;
			String ClassName2 = Class2;
			if( numberOfClass1 > numberOfClass2){
				cut = (double)numberOfClass2/(double)numberOfClass1;
				counter1 = numberOfClass2;
				counter2 = numberOfClass1;
				ClassName1 = Class2;
				ClassName2 = Class1;
			}
			for(int i=0; i< classes.length; i++){
				if (Math.random() < cut)
					if(counter1 == 0){
						classes[i] = ClassName2;
						counter2--;
					}else{					
						classes[i] = ClassName1;
						counter1--;
					}
				else{ 
					if(counter2 == 0){
						classes[i] = ClassName1;
						counter1--;
					}else{					
						classes[i] = ClassName2;
						counter2--;
					}
				}
			}
			*/
		}
		//Code to support the Absent Present files
		double[][] apdata = null;
		int genesWithAbsent =0;
		File apFile = new File(inputFileName+".ap");
		if(apFile.exists() && !apFile.isDirectory()) {
			apdata = readAP(apFile, data.length, data[0].length);   //Generic function. Can be found in the FS class
			apfilexists = true;
		}
		double[][] tmp;
		for(int i=0; i<genes.length; i++){
			if(apfilexists){
				int countA = 0;
				for(int j=0; j<apdata[i].length; j++)
					if(apdata[i][j] == 0)
						countA++;
				if(countA > apdata[i].length/10 ){
					data[i] = apdata[i].clone();
					genesRate[i] = 0.5;
					if(ranking)
						geneRanking(i, genesRate[i], data);
					genesWithAbsent++;
				}
				else{
					QuickSort q = new QuickSort();
					tmp = q.quicksort(setGeneAndClass(i));
					genesRate[i] = caclulateBestInfoGainCut(tmp);
					if(ranking)
						geneRanking(i, genesRate[i], tmp);
				}
			}else{
				QuickSort q = new QuickSort();
				tmp = q.quicksort(setGeneAndClass(i));
				genesRate[i] = caclulateBestInfoGainCut(tmp);
				if(ranking)
					geneRanking(i, genesRate[i], tmp);
			}
			
			if(DEBUGmode == 1)
				System.out.println("Threshold for Gene "+genes[i] +" is :" +genesRate[i]);
		}
		if(apfilexists)
			System.out.println("Genes with more than 10% Absent in the expression values:" +genesWithAbsent);
			
		//printAll(true);
		saveDiscreteFile(outputFileName);
		
		if(ranking)
			saveNBestDiscreteFile(outputFileName+"-Best.txt", N);
	}
	

	public void startMetric(String inputFileName) throws IOException{
		startMetric(inputFileName, inputFileName+".discr", 50, false, false);
	}
	
	public void startMetricPermutation(String inputFileName) throws IOException{
		startMetric(inputFileName, inputFileName+".discr", 50, false, true);
	}

	private void geneRanking(int index, double median,double[][] geneData){
		int i,j;

			int countClass1under=0, countClass1over=0, countClass2under=0, countClass2over = 0;
			for(i=0; i<geneData[0].length; i++){
				if(geneData[1][i] == 1) //Class 1
					if(geneData[0][i] < median)
						countClass1under++;
					else
						countClass1over++;
				else
					if(geneData[0][i] < median)
						countClass2under++;
					else
						countClass2over++;
			}
			if(discrFilter) 	//Discritization filter
				simplepPredictor[index] = (countClass1over * countClass2under - countClass1under * countClass2over);
			else{		//Polarity filter
				
				simplepPredictor[index] = (double)(countClass1over - countClass2over)/(double)(countClass1over + countClass2over);
				
				/** Old polarity filter
				 * 
				simplepPredictor[index] = (double)(countClass1over - countClass2over)/(double)(countClass1over + countClass2over);
				int total = countClass1over + countClass2under + countClass1under + countClass2over;
				if(Math.abs(simplepPredictor[index]) >= ((double)(countClass1over + countClass2over)/total) )
					filter2[index] = true;
				else
					filter2[index] = false;
				//System.out.println(simplepPredictor[index] +"\t" + filter2[index]);
				 * 
				 */
			}
			//		/ ((countClass1over+countClass1under)*(countClass2over+countClass2under) ); //If you want to "normalize it to [0-1] per class
			
	}
	
	
	public void  saveNBestDiscreteFile(String fileName, int N) throws IOException{
		
		if(DEBUGmode == 1)
			System.out.print("Writing N best Features (Discretized data) to "+fileName +" ");
		
		double posMax, posMin, negMax, negMin;
		
		if(simplepPredictor.length < 150)
			return;
		double[] sorted = simplepPredictor.clone();
		Arrays.sort(sorted);
		negMax = sorted[0];
		negMin = sorted[N];
		posMax = sorted[sorted.length-1];
		posMin = sorted[sorted.length -N];
		
		BufferedWriter writer = new BufferedWriter( new FileWriter(fileName) );
		BufferedWriter writerPlus50 = new BufferedWriter( new FileWriter(fileName.substring(0, fileName.length()-12) +"PlusBest.txt") );
		BufferedWriter writerMinus50 = new BufferedWriter( new FileWriter(fileName.substring(0, fileName.length()-12) +"MinusBest.txt") );

		String lineToWrite = new String();
		
		lineToWrite = lineToWrite.concat("GeneID\t");
		
		lineToWrite = lineToWrite.concat("Ranking\t");
		
		for(int i=0; i < classes.length;i++)
			lineToWrite = lineToWrite.concat(classes[i]+"\t");
		writer.write(lineToWrite);
		writerPlus50.write(lineToWrite);
		writerMinus50.write(lineToWrite);
		lineToWrite = "";

		for(int i=0; i<genes.length; i++){
			if(discrFilter){
				if( (simplepPredictor[i] > negMin) && (simplepPredictor[i] < posMin) )
					continue;
				lineToWrite = lineToWrite.concat("\n" +genes[i] +"\t");			
				lineToWrite = lineToWrite.concat(simplepPredictor[i] +"\t");			
				for(int j=0; j<data[i].length; j++){
					if(data[i][j] > genesRate[i])
						lineToWrite = lineToWrite.concat("1\t");
					else
						lineToWrite = lineToWrite.concat("0\t");
				}
				writer.write(lineToWrite);
				if(simplepPredictor[i] >= posMin)				
					writerPlus50.write(lineToWrite);
				else
					writerMinus50.write(lineToWrite);
				lineToWrite = "";
			}else{
				int zeroIndex = Arrays.binarySearch(sorted, 0);
				double[] posArray = Arrays.copyOfRange(sorted, zeroIndex, sorted.length);
				double[] negArray = Arrays.copyOfRange(sorted, 0, zeroIndex);

				double averPos = StdStats.sum(posArray)/posArray.length;
				double averNeg = StdStats.sum(negArray)/negArray.length;
				
				System.out.println(averPos +"\t" +averNeg +"\t" +simplepPredictor[i]);
				
				if( (simplepPredictor[i] > averNeg) && (simplepPredictor[i] < averPos) ) //Filter 1 if is less than the average ignore
					continue;

				System.out.println("Passed !");
				lineToWrite = lineToWrite.concat("\n" +genes[i] +"\t");			
				lineToWrite = lineToWrite.concat(simplepPredictor[i] +"\t");			
				for(int j=0; j<data[i].length; j++){
					if(data[i][j] > genesRate[i])
						lineToWrite = lineToWrite.concat("1\t");
					else
						lineToWrite = lineToWrite.concat("0\t");
				}
				writer.write(lineToWrite);
				if(simplepPredictor[i] >= averPos)
					writerPlus50.write(lineToWrite);
				else
					writerMinus50.write(lineToWrite);
				lineToWrite = "";
			}
		}
		
		writer.flush();
		writer.close();
		writerPlus50.flush();
		writerPlus50.close();
		writerMinus50.flush();
		writerMinus50.close();
		if(DEBUGmode == 1)
			System.out.println("... Done.");

	}

}
