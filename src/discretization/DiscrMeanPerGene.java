package discretization;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import misc.StdStats;


/**
 * 
 * @author koumakis
 *
 */
public class DiscrMeanPerGene extends FS{
	
	public DiscrMeanPerGene(int DebugMode, boolean addRanking){
		DEBUGmode = DebugMode;
		numberOfClass1 = 0;
		numberOfClass2 = 0;
		ranking = addRanking;
	}
	
	public DiscrMeanPerGene(int DebugMode){
		DEBUGmode = DebugMode;
		numberOfClass1 = 0;
		numberOfClass2 = 0;
	}
	
	public void startMetric(String inputFileName, String outputFileName) throws IOException{
		//genes and data variables have been constructed by readData() in the FS class.
		readData(inputFileName);
		
		//Code to support the Absent Present files
		double[][] apdata = null;
		int genesWithAbsent =0;
		File apFile = new File(inputFileName+".ap");
		if(apFile.exists() && !apFile.isDirectory()) {
			apdata = readAP(apFile, data.length, data[0].length);   //Generic function. Can be fund in the FS class
			apfilexists = true;
		}
		for(int i=0; i<genes.length; i++){
			if(apfilexists){
				int countA = 0;
				for(int j=0; j<apdata[i].length; j++)
					if(apdata[i][j] == 0)
						countA++;
				if(countA > apdata[i].length/10 ){
					data[i] = apdata[i].clone();
					genesRate[i] = 0.5;
					genesWithAbsent++;
				}
				else
					genesRate[i] = mean(data[i]);
			}else
				genesRate[i] = mean(data[i]);
		
		// Old code wihout AP file
		//	for(int i=0; i<genes.length; i++){
		//		genesRate[i] = kmeansPerGene(data[i]);
			
			if(DEBUGmode == 1)
				System.out.println("Threshold for Gene "+genes[i] +" is :" +genesRate[i]);
		}
		if(apfilexists)
			System.out.println("Genes with more than 10% Absent in the expression values:" +genesWithAbsent);
	}
	
	
	public void startMetric(String inputFileName) throws IOException{
		startMetric(inputFileName, inputFileName+".discr");
	}

	public static double mean(double[] m) {
	    double sum = 0;
	    for (int i = 0; i < m.length; i++) {
	        sum += m[i];
	    }
	    return sum / m.length;
	}
}