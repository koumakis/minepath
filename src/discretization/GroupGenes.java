package discretization;
import java.util.ArrayList;
import java.util.List;

public class GroupGenes {

	public static int DEBUGmode;
	private int geneRatedThreshold;
	boolean[] isInList;
	FSadditive fs;
//	FSmultiply fs;
	List group= new ArrayList();
	double[][] sortedGenes;
	double diff = Double.MIN_VALUE;

	
	public GroupGenes(FSadditive _fs, int DebugMode){
		fs = _fs;
		DEBUGmode = DebugMode;
		int[] geneId = new int[fs.genes.length];
		isInList = new boolean[fs.genes.length];
		
		for(int i=0; i<geneId.length; i++){
			geneId[i] = i;
			isInList[i] = false;
		}
		calculate(geneId);
	}

	public GroupGenes(FSmultiply _fs, int DebugMode){
//		fs = _fs;
		DEBUGmode = DebugMode;
		int[] geneId = new int[fs.genes.length];
		isInList = new boolean[fs.genes.length];
		
		for(int i=0; i<geneId.length; i++){
			geneId[i] = i;
			isInList[i] = false;
		}
		calculate(geneId);
	}
	
	private void calculate(int[] geneId){
		if(DEBUGmode == 1)
			System.out.print("Started the Grouping of Genes.\nSorting Genes according to the Rate ...");
		sortedGenes = sortGenes(geneId);
		if(DEBUGmode == 1)
			System.out.println("...Done.\nAdding the first Gene to the Group List and generating the group");

		geneRatedThreshold  = setGenesThreshold(Integer.MAX_VALUE);
		System.out.println("Threshold is :"+geneRatedThreshold);
		
		group.add(new Integer((int)sortedGenes[1][0]) );
		isInList[0] = true;

		boolean res = true;
		int iterations = 0;
		while((res == true)  ){
			iterations++;
			res = appendNextBest();
			if(DEBUGmode == 1){
				System.out.println("Iteration :"+iterations);
				printGroup();
			}
		}
		System.out.println("\n\nFinal Group of genes is :");
		printGroupWithData();
	}

	private void printGroup(){
		System.out.println("The Best Group for the Feature Selection is :");
		for(int i=0; i<group.size();i++)
			System.out.println( fs.genes[((Integer)group.get(i)).intValue()]  );
	}

	private void printGroupWithData(){
		System.out.println("The Best Group for the Feature Selection is :");
		for(int i=0; i<group.size();i++){
			int index = ((Integer)group.get(i)).intValue();
			System.out.print(fs.genes[index]);
			for(int j=0; j<fs.classes.length; j++)
				System.out.print("\t"+fs.data[index][j]);
			System.out.println();
		}
	}
	
	private boolean appendNextBest(){
		double[][] curGene= list2sortedGeneValues(group);;
		double bestScore = fs.calculateGeneRate(curGene);
//		if(bestScore == 0) return false;

		int bestToAdd = -1;
		boolean ret = false;
		List newGroup = group.subList(0, group.size());

		for(int i=0; i<geneRatedThreshold; i++){
			if(isInList[i] == false){
				newGroup.add( (new Integer( (int)sortedGenes[1][i])));
				curGene = list2sortedGeneValues(newGroup);
				double newScore = fs.calculateGeneRate(curGene);
				if(newScore < bestScore){
					bestScore = newScore;
					bestToAdd = i;
					ret = true;
					if(bestScore == 0){
						newGroup.remove(newGroup.size()-1);
						break;
					}
				}

				else if(newScore == bestScore){
					//TODO : search for the maximum distance between the two classes.
					int k=1;
					while(curGene[1][k-1] == curGene[1][k]){ 		// Search the change at class.
						k++;
					}
					if(diff < Math.abs(curGene[0][k-1] - curGene[0][k])/newGroup.size() ){
						diff = Math.abs(curGene[0][k-1] - curGene[0][k])/newGroup.size();
						bestScore = newScore;
						bestToAdd = i;
						ret = true;	
					}
				
				}

				newGroup.remove(newGroup.size()-1);
			}
		}
		if(ret == true){
			group.add(new Integer((int)sortedGenes[1][bestToAdd]));
			System.out.println("\n"+isInList[bestToAdd] +"\t"+bestToAdd);
			isInList[bestToAdd] = true;
			System.out.println("Adding Gene "+bestToAdd+"\t"+fs.genes[(int)sortedGenes[1][bestToAdd]]+"\twith score :"+bestScore+"\t and Diff = "+diff);
		}
		return(ret);
	}

	private double[][] list2sortedGeneValues(List l){
		double[][] tmp = new double[2][fs.classes.length];
		for(int i=0; i<fs.classes.length;i++){
			if(fs.classes[i].compareTo(FS.Class1) == 0) {
				tmp[1][i] = 1;
			} else
				tmp[1][i] = 2;
			tmp[0][i] = 0;
			for(int j=0; j<l.size(); j++){
				int gId =((Integer)l.get(j)).intValue();
				tmp[0][i] += fs.data[gId][i];
			}
		}
		QuickSort q = new QuickSort();
		tmp = q.quicksort(tmp);
		return (tmp);
	}
	
	/*
	private double score(List l){
		double[][] tmp = new double[2][fs.classes.length];
		for(int i=0; i<fs.classes.length;i++){
			if(fs.classes[i].compareTo(FS.Class1) == 0) {
				tmp[1][i] = 1;
			} else
				tmp[1][i] = 2;
			tmp[0][i] = 0;
			for(int j=0; j<l.size(); j++){
				int gId =((Integer)l.get(j)).intValue();
				tmp[0][i] += fs.data[gId][i];
			}
		}
		QuickSort q = new QuickSort();
		tmp = q.quicksort(tmp);
		//TODO : get the distance between the two classes.

		return (fs.calculateGeneRate(tmp));
	}
*/
	private double[][] sortGenes(int[] geneId){
		double[][] ret = new double[2][geneId.length];
		for (int i=0; i<geneId.length; i++){
			ret[0][i] = fs.genesRate[i];
			ret[1][i] = geneId[i];
		}
		QuickSort q = new QuickSort();
		ret = q.quicksort(ret);
		return(ret);
	}
	
	private int setGenesThreshold(int maxPercentage){
		double bestsRate = sortedGenes[0][0]; 
		//TODO: Next line is temporary. ALL-AML domain have one Gene with Rate 0.
		if(bestsRate == 0)	bestsRate = 0.001;

		int i;
		System.out.println("Sorted Genes.");
		for(i=0; i<sortedGenes[0].length; i++){
			System.out.println(sortedGenes[0][i]);
			if( ((sortedGenes[0][i]/bestsRate)-1)*100 > maxPercentage   ){
				break;
			}
		}
		return i;
	}

}
