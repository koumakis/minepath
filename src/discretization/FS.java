package discretization;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import misc.saveToFile;

public class FS{

	public static String Class1 ;
	public static String Class2 ;
	public static int DEBUGmode;
	
	public String[] classes;
	public String[] genes;
	public double[][] data;
	public int numberOfClass1;
	public int numberOfClass2;
	public double[] genesRate;
	
	
	
	protected double simplepPredictor[];
	protected boolean ranking = false;
	protected boolean apfilexists = false;
	//protected boolean filter2[];
	
	
	public String[] getClasses(){
		return(classes);
	}
	
	public String[] getGenes(){
		return(genes);
	}
	
	public double[][] getData(){
		return(data);
	}
	public double[] getGenesRate(){
		return(genesRate);
	}


	
	/**
	 * Reads a dataset file. The dataset must be tab delimeted. 
	 * The first row contains the 2 classes
	 * The first column contains the Gene names.
	 * @param sf
	 * @param _classes 
	 * @throws IOException
	 */
	protected void readData(saveToFile sf, String[] _classes) throws IOException{
		classes = _classes;
		int numOfSubpaths = sf.mergedData.size()+sf.mergedNonFunctionalSubPaths.size();
		genes = new String[numOfSubpaths];
		genesRate = new double[numOfSubpaths];
		simplepPredictor = new double[numOfSubpaths];
		data = new double[numOfSubpaths][classes.length];
		Set set = sf.mergedData.entrySet(); 
		Iterator iter = set.iterator(); 
		int g = 0;
		while(iter.hasNext()) { 
			Map.Entry me = (Map.Entry)iter.next(); 
			//writer.write("\n" +(String)me.getKey() +"\t");
			BitSet b = (BitSet)me.getValue();
			genes[g] = (String)me.getKey();
			
			if(b.isEmpty()){
				for(int i=0; i<classes.length-b.length(); i++)
					data[g][i] = 0;
					//writer.write("0\t");
				continue;
			}
			for(int i=0; i<b.length(); i++){
				if(b.get(i))
					data[g][i] = 1;
					//writer.write("1\t");
				else
					data[g][i] = 0;
					//writer.write("0\t");
			}
			for(int i=0; i<classes.length-b.length(); i++) //At BitSet the system ignores the last falses which for us are zeros...
				data[g][b.length()+i] = 0;
				//writer.write("0\t");
			g++;
		}
		
		for(int i=0; i<sf.mergedNonFunctionalSubPaths.size(); i++){
			genes[g+i] = sf.mergedNonFunctionalSubPaths.get(i);
			//writer.write("\n" +sf.mergedNonFunctionalSubPaths.get(i) +"\t");
			for(int j=0; j<classes.length; j++)
				data[g+i][j] = 0;
				//writer.write("0\t");
		}
	}
	
	protected void readData(String fileName) throws IOException{

		File f = new File(fileName);
		if (f.exists() == false){
			System.out.println("File "+fileName+" doesn't exist.");
			return;
		}
		BufferedReader reader = new BufferedReader(new FileReader(f));
		String line;
		// Read the rows and the colouns.
		line = reader.readLine();
		String[] row = line.split("\t");

		int counter =0;
		while ( (line = reader.readLine()) != null) {
			counter++;
		}

		int rows = (new Integer(counter));
		int cols = (new Integer(row.length-1));

		String[] tmp = row;
		classes = new String[cols];
		for(int j=1; j<tmp.length; j++)
			classes[j-1] = tmp[j];		
		
		Class1 = classes[0];
		int tmpIndexFinder = classes.length-1;
		String tmpClassFinder = classes[tmpIndexFinder];		
		while(Class1.compareTo(tmpClassFinder) == 0){
			tmpIndexFinder--;
			tmpClassFinder = classes[tmpIndexFinder];
			
		}
		Class2 = tmpClassFinder;
		
		genes = new String[rows];
		data = new double[rows][cols];
		genesRate = new double[rows];
		//TODO: delete it. Just for test
		simplepPredictor = new double[rows];
		//filter2 = new boolean[rows];
		
		if(DEBUGmode == 1){
			System.out.println("Total Genes :"+genes.length);
			System.out.println("Total Cases :"+classes.length );
			System.out.println("Class 1 :"+Class1 +"\tClass 2 :" +Class2);
			System.out.print("Starting Parsing Data Matrix ...");
		}
		int i=0;
		for(i=0; i<classes.length; i++)
			if(classes[i].compareTo(Class1) == 0 )
				numberOfClass1++;
			else if(classes[i].compareTo(Class2) == 0)
				numberOfClass2++;
			else
				System.out.println("Unknown Class :"+classes[i]);
		if(DEBUGmode == 1){
			System.out.println("... Done.");
			System.out.println("Number of Instances in Class "+Class1 +" is :"+numberOfClass1);
			System.out.println("Number of Instances in Class "+Class2 +" is :"+numberOfClass2);
		}
		
		reader.close();
		
		//Reopen and read the data.
		reader = new BufferedReader(new FileReader(f));

		line = reader.readLine();

		String[] splited;
		i=0;
		while ((line = reader.readLine()) != null) {
			splited = line.split("\t");
			genes[i] = splited[0];
			for(int j=0; j<splited.length-1; j++)
				try{
					data[i][j] = (new Double(splited[j+1])).doubleValue();
				}catch(NumberFormatException ex){
					System.out.println("At data "+i +" "+j +" Number Format Exception.");
					data[i][j] = Double.NaN;
				}
			i++;
		}
		reader.close();
	}
	
	protected double[][] setGeneAndClass(int geneIndex){
		double[][] ret = new double[2][data[0].length];
		
		for (int i=0; i<data[0].length; i++){
			ret[0][i] = data[geneIndex][i];
			if(classes[i].compareTo(Class1) == 0)
				ret[1][i] = 1;
			else
				ret[1][i] = 2;
		}
		return(ret);
	}

	/**
	 *  computes the best split value which is a midpoint between two (sorted) values and gives the best
	 * Information gain (according to Entropy theory) for the 2 classes
	 * @param geneData A sorted table according to values on geneData[1] and on geneData[0] we have the class
	 * @return
	 */
	public double caclulateBestInfoGainCut(double[][] geneData){
		double ret= Double.NaN;
		int i,j;
		double curMedian =0;
		double maxInfoGain = Double.MIN_VALUE;
		int flagForExtreme = 0;

		for(j=0; j<geneData[0].length-1; j++){
			curMedian = (geneData[0][j] +geneData[0][j+1])/2;
			
			double countClass1under=0, countClass1over=0, countClass2under=0, countClass2over = 0;
			for(i=0; i<geneData[0].length; i++){			
				if(geneData[1][i] == 1) //Class 1
					if(geneData[0][i] < curMedian)
						countClass1under++;
					else
						countClass1over++;
				else
					if(geneData[0][i] < curMedian)
						countClass2under++;
					else
						countClass2over++;
			}
			double total = countClass1under + countClass1over + countClass2under + countClass2over;
			double countClass1 =  countClass1under + countClass1over;
			double countClass2 =  countClass2under + countClass2over;
			double totalUnder  =  countClass1under + countClass2under;
			double totalOver   =  countClass1over  + countClass2over;
			if(total == 0)
				flagForExtreme = 2;
			else if(totalUnder==0)
				flagForExtreme = -1;
			else if(totalOver == 0)
				flagForExtreme = 2;
			
			double entropy = -1 * (countClass1/total) *myLog2(countClass1/total) - (countClass2/total)*myLog2(countClass2/total);
			double entropyUnder = -1* (countClass1under/totalUnder) * myLog2(countClass1under/totalUnder) 
					- (countClass2under/totalUnder) * myLog2(countClass2under/totalUnder);
			double entropyOver =  -1* (countClass1over/totalOver) * myLog2(countClass1over/totalOver) 
					- (countClass2over/totalOver) * myLog2(countClass2over/totalOver);
			double curEntropy = ((countClass1under + countClass2under)/total) * entropyUnder +
					((countClass1over + countClass2over)/total) * entropyOver;
					
			double infoGain = entropy - curEntropy;
			
			if (infoGain > maxInfoGain){
				maxInfoGain = infoGain;
				ret = curMedian;
			}
		}
		if (Double.isNaN(ret)){ // if all data are 0 or all data are 1 
			return flagForExtreme;
		}
		//Return the median with highest Information Gain
		return ret;
	}
	
	private double myLog2(double value){
		if (value ==0)
			return 0;
		else
			return Math.log(value)/Math.log(2); 
	}
	
	public void  saveDiscreteFile(String fileName) throws IOException{
		
		if(DEBUGmode == 1)
			System.out.print("Writing Discretized data file to "+fileName +" ");
		
		BufferedWriter writer = new BufferedWriter( new FileWriter(fileName) );
		writer.write("GeneID\t");
		BufferedWriter writerThr = new BufferedWriter( new FileWriter("thresholds") );
		writerThr.write("GeneID\tDiscr.Threshold");
		
		if(ranking)
			writer.write("Discr.Threshold\tRanking\t");
		
		for(int i=0; i < classes.length;i++)
			writer.write(classes[i]+"\t");
		for(int i=0; i<genes.length; i++){
			writer.write("\n" +genes[i] +"\t");
			writerThr.write("\n" +genes[i] +"\t");
			
			if(ranking)
				writer.write(genesRate[i] +"\t" +simplepPredictor[i]+"\t");
			
			for(int j=0; j<data[i].length; j++){


				if(data[i][j] > genesRate[i])
					writer.write("1\t");
				else
					writer.write("0\t");
			}
			writerThr.write(""+genesRate[i]);
		}
		
		writer.flush();
		writer.close();
		writerThr.flush();
		writerThr.close();
		if(DEBUGmode == 1)
			System.out.println("... Done.");

	}
	
	protected double[][] readAP(File apFile, int matrixRows, int matrixCols) throws IOException {
		double[][] ret = new double[matrixRows][matrixCols];
		BufferedReader reader = new BufferedReader(new FileReader(apFile));
		String line;	
		//Ignore first line with the class names
		line = reader.readLine();
		String[] row = line.split("\t");
		int geneIndex=0;
		while ((line = reader.readLine()) != null) {
			row = line.split("\t");

			for(int i=1; i<row.length; i++){  //First element at row[0] is the gene name which we ignore at the data matrix. We have to store it at i-1
				if( (row[i].compareTo("P") == 0) || (row[i].compareTo("M") == 0) )
					ret[geneIndex][i-1] = 1;
				else
					ret[geneIndex][i-1] = 0;
			}
			geneIndex++;
		}
		reader.close();
		//Debug only
		/* 
		 for(int i=0; i<ret.length; i++){
			System.out.print(genes[i]);
			for(int j=0; j<ret[i].length; j++)
				System.out.print("\t" +ret[i][j]);
			System.out.println();
		}
		*/
		return ret;
	}
	
	
	public boolean[][] getDiscritezedData(){
		boolean[][] ret= new boolean[data.length][data[0].length];
		for(int i=0; i<genes.length; i++){		
			for(int j=0; j<data[i].length; j++){
				if(data[i][j] > genesRate[i])
					ret[i][j] = true;
				else
					ret[i][j] = false;
			}
		}
		return ret;
	}
	
	public void printGene(int geneIndex){
		System.out.print(genes[geneIndex]+"\t");
		for(int i=0; i<data[geneIndex].length; i++)
			System.out.print(data[geneIndex][i]+"\t");
		System.out.println(genesRate[geneIndex]);		
	}
	
	public void printDiscrGene(int geneIndex){
		System.out.print(genes[geneIndex]+"\t");
		for(int i=0; i<data[geneIndex].length; i++)
			if(data[geneIndex][i] >genesRate[geneIndex])
				System.out.print("1\t");
			else
				System.out.print("0\t");
		System.out.println();
	}
	
	public void printAll(boolean discretized){
		for(int i=0; i < classes.length;i++)
			System.out.print(classes[i]+"\t");
		System.out.println();
		for(int i=0; i<genes.length; i++)
			if(discretized)
				printDiscrGene(i);
			else
				printGene(i);
	}

	public void startMetric(String string) throws IOException{ //Overridden method 
		// TODO Auto-generated method stub
	}
	public void startMetricPermutation(String string) throws IOException{ //Overridden method 
		// TODO Auto-generated method stub
	}
	
}

class MAdataSerialized implements Serializable{
	public String Class1;
	public String Class2;
	public String[] classes;
	public String[] genes;
	public boolean[][] discrData;
	public int numberOfClass1;
	public int numberOfClass2;

	MAdataSerialized (String _Class1, String _Class2, String[] _classes, String[] _genes, boolean[][] _discrData, int _numberOfClass1, int _numberOfClass2){
		Class1 = _Class1;
		Class2 = _Class2;
		classes = _classes;
		genes = _genes;
		discrData = _discrData;
		numberOfClass1 = _numberOfClass1;
		numberOfClass2 = _numberOfClass2;
	}
	
	public void serializeThis(String fileName) throws IOException {
        FileOutputStream fos = new FileOutputStream(fileName +".discr.ma");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(this);
        oos.close();
    }
}