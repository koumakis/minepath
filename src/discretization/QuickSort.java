package discretization;
/*************************************************************************
 *  Compilation:  javac QuickSort.java
 *  Execution:    java QuickSort N
 *  
 *  Generate N random real numbers between 0 and 1 and quicksort them.
 *
 *  On average, this quicksort algorithm runs in time proportional to
 *  N log N, independent of the input distribution. The algorithm
 *  guards against the worst-case by randomly shuffling the elements
 *  before sorting. In addition, it uses Sedgewick's partitioning
 *  method which stops on equal keys. This protects against cases
 *  that make many textbook implementations, even randomized ones,
 *  go quadratic (e.g., all keys are the same).
 *
 *************************************************************************/

public class QuickSort {

   /***********************************************************************
    *  Quicksort code from Sedgewick 7.1, 7.2.
    ***********************************************************************/
    public double[][] quicksort(double[][] a) {
        quicksort(a, 0, a[0].length - 1);
        return(a);
    }

    private void quicksort(double[][] a, int left, int right) { 
        if (right <= left) return;
        int i = partition(a, left, right);
        quicksort(a, left, i-1);
        quicksort(a, i+1, right);
    }

    private  int partition(double[][] a, int left, int right) {
        int i = left - 1;
        int j = right;
        while(true) { 
            while (less(a[0][++i], a[0][right]))       	// find item on left to swap
                ;                                      	// a[right] acts as sentinel
            while (less(a[0][right], a[0][--j]))       	// find item on right to swap
                if (j == left) break;            		// don't go out-of-bounds
            if (i >= j) break;                   		// check if pointers cross
            exch(a, i, j);                       		// swap two elements into place
        }
        exch(a, i, right);                       		// swap with partition element
        return i;
    }

    // is x < y ?
    private  boolean less(double x, double y) {
        return (x < y);
    }

    // exchange a[i] and a[j]
    private  void exch(double[][] a, int i, int j) {
        double swap0 = a[0][i];
        a[0][i] = a[0][j];
        a[0][j] = swap0;

        double swap1 = a[1][i];
        a[1][i] = a[1][j];
        a[1][j] = swap1;

    }
}
