package discretization;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import weka.clusterers.SimpleKMeans;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;


/**
 * 
 * @author koumakis
 *
 */
public class DiscrKmeans extends FS{
	
	
	public DiscrKmeans(int DebugMode, boolean addRanking){
		DEBUGmode = DebugMode;
		numberOfClass1 = 0;
		numberOfClass2 = 0;
		ranking = addRanking;
	}
	
	public DiscrKmeans(int DebugMode){
		DEBUGmode = DebugMode;
		numberOfClass1 = 0;
		numberOfClass2 = 0;
	}
	
	public void startMetric(String inputFileName, String outputFileName, boolean permutation) throws IOException{
		
		//genes and data variables have been constructed by readData() in the FS class.
		readData(inputFileName);
		
		if(permutation){
			for(int i=0; i< classes.length; i++){
				if (Math.random() < 0.5)
					if(numberOfClass1 == 0){
						classes[i] = Class2;
						numberOfClass2--;
					}else{					
						classes[i] = Class1;
						numberOfClass1--;
					}
				else{ 
					if(numberOfClass2 == 0){
						classes[i] = Class1;
						numberOfClass1--;
					}else{					
						classes[i] = Class2;
						numberOfClass2--;
					}
				}
			}
		}
		//Code to support the Absent Present files
		double[][] apdata = null;
		int genesWithAbsent =0;
		File apFile = new File(inputFileName+".ap");
		if(apFile.exists() && !apFile.isDirectory()) {
			apdata = readAP(apFile, data.length, data[0].length);   //Generic function. Can be fund in the FS class
			apfilexists = true;
		}
		for(int i=0; i<genes.length; i++){
			if(apfilexists){
				int countA = 0;
				for(int j=0; j<apdata[i].length; j++)
					if(apdata[i][j] == 0)
						countA++;
				if(countA > apdata[i].length/10 ){
					data[i] = apdata[i].clone();
					genesRate[i] = 0.5;
					genesWithAbsent++;
				}
				else
					genesRate[i] = kmeansPerGene(data[i]);
			}else
				genesRate[i] = kmeansPerGene(data[i]);
		
		// Old code wihout AP file
		//	for(int i=0; i<genes.length; i++){
		//		genesRate[i] = kmeansPerGene(data[i]);
			
			if(DEBUGmode == 1)
				System.out.println("Threshold for Gene "+genes[i] +" is :" +genesRate[i]);
		}
		if(apfilexists)
			System.out.println("Genes with more than 10% Absent in the expression values:" +genesWithAbsent);
			
		//printAll(true);
		saveDiscreteFile(outputFileName);
	}
	

	public void startMetric(String inputFileName) throws IOException{
		startMetric(inputFileName, inputFileName+".discr", false);
	}

	public void startMetricPermutation(String inputFileName) throws IOException{ //Overridden method 
		startMetric(inputFileName, inputFileName+".discr", true);
	}
	
	public double kmeansPerGene(double[] expressionValues) {
	    double thrs  =-1;
	    
	    //Instances dataset = null;
	    
	    java.util.ArrayList<Attribute> fvWekaAttributes = new ArrayList<Attribute>();

	    Attribute Attribute1 = new Attribute(new String("Gene"));
   		fvWekaAttributes.add(Attribute1);    
	    Instances dataset = new Instances("dataset", fvWekaAttributes, 0);   
	    
	    
	    for(int i=0; i<expressionValues.length; i++){
	    	double[] tmp = new double[1];
	    	tmp[0] = expressionValues[i];
	    	Instance inst = new DenseInstance(1, tmp);
		    inst.setDataset(dataset); 
		    dataset.add(inst);
	    }
	    
	    SimpleKMeans kMeans = new SimpleKMeans();
	    try {
			kMeans.setNumClusters(2);
			kMeans.buildClusterer(dataset);
			
			// print out the cluster centroids
			Instances centroids = kMeans.getClusterCentroids();
			thrs = (centroids.instance(0).value(0) + centroids.instance(1).value(0))/2;
			/*
			for (int i = 0; i < centroids.numInstances(); i++)
			  System.out.print("Centroid " +(i + 1)+": "+centroids.instance(i) +"\t");
			// get cluster membership for each instance
			System.out.println();
			for (int i = 0; i < dataset.numInstances(); i++) {
				System.out.println( dataset.instance(i)+" is in cluster " +kMeans.clusterInstance(dataset.instance(i)) + 1 );
			}
			*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Discretization process failed. Exiting.");
			e.printStackTrace();
			System.exit(1);
		}
	    return thrs;
	}
	
}