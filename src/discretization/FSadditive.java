package discretization;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FSadditive extends FS{


	public FSadditive(int DebugMode){
		DEBUGmode = DebugMode;
		numberOfClass1 = 0;
		numberOfClass2 = 0;
	}

	public void startMetric(String fileName) throws IOException{
		readData(fileName);

		double[][] tmp;
		for(int i=0; i<genes.length; i++){
			QuickSort q = new QuickSort();
			tmp = q.quicksort(setGeneAndClass(i));
			if(DEBUGmode == 1)
				System.out.println("Calculating Rate for Gene :"+genes[i]);
			genesRate[i] = calculateGeneRate(tmp);
		}
	}
	
	/* Inherited
	private double[][] setGeneAndClass(int geneIndex){
		double[][] ret = new double[2][data[0].length];
		
		for (int i=0; i<data[0].length; i++){
			ret[0][i] = data[geneIndex][i];
			if(classes[i].compareTo(Class1) == 0)
				ret[1][i] = 1;
			else
				ret[1][i] = 2;
		}
		return(ret);
	}
	*/
	
	public double calculateGeneRate(double[][] geneData){

		double ret =  0;
		int curClass;
		int index = 0;
		
		while(index < geneData[0].length){
			int inRowSameClass = 0;
			curClass = (int)geneData[1][index];
			
			while((curClass == geneData[1][index] ) ){
				inRowSameClass++;
				index++;
				if(index >= geneData[0].length)
					break;			
			}
			if(curClass == 1 ){		//Can change the formula from inRowSameClass to 1/inRowSameClass
				ret = ret + inRowSameClass*Math.abs(Math.log10( (double)(inRowSameClass)/(double)(numberOfClass1) ) );
			}else if(curClass == 2 ){
				ret = ret + inRowSameClass*Math.abs(Math.log10( (double)(inRowSameClass)/(double)(numberOfClass2) ) );
			}else
				System.out.println("In CalculateGene Unknown Class :"+curClass);

			if(index >= geneData[0].length)
				break;
		}
		return ret;
	}
	
	public void printGene(int geneIndex){
		System.out.print(genes[geneIndex]+"\t");
		for(int i=0; i<data[geneIndex].length; i++)
			System.out.print(data[geneIndex][i]+"\t");
		System.out.println(genesRate[geneIndex]);		
	}
	
	public void printAll(){
		for(int i=0; i < classes.length;i++)
			System.out.print(classes[i]+"\t");
		System.out.println();
		for(int i=0; i<genes.length; i++)
			printGene(i);
	}
	
	/* Inherited
	private void readData(String fileName) throws IOException{

		File f = new File(fileName);
		if (f.exists() == false){
			System.out.println("File "+fileName+" doesn't exist.");
			return;
		}
		BufferedReader reader = new BufferedReader(new FileReader(f));
		String line;
		// Read the rows and the colouns.
		line = reader.readLine();
		String[] row_cols = line.split("\t");
		int rows = (new Integer(row_cols[0])).intValue();
		int cols = (new Integer(row_cols[1])).intValue();
		line = reader.readLine();
		String[] tmp = line.split("\t");
		classes = new String[cols];
		for(int j=1; j<tmp.length; j++)
			classes[j-1] = tmp[j];		
		genes = new String[rows];
		data = new double[rows][cols];
		genesRate = new double[rows];
		if(DEBUGmode == 1){
			System.out.println("Number of ROWS is :"+rows);
			System.out.println("Number of COLS is :"+cols);
			System.out.println("Total Genes :"+genes.length);
			System.out.println("Total Cases :"+classes.length);
			System.out.print("Starting Parsing Data Matrix ...");
		}
		String[] splited;
		int i=0;
		while ((line = reader.readLine()) != null) {
			splited = line.split("\t");
			genes[i] = splited[0];
			for(int j=0; j<splited.length-1; j++)
				try{
					data[i][j] = (new Double(splited[j+1])).doubleValue();
				}catch(NumberFormatException ex){
					System.out.println("At data "+i +" "+j +" Number Format Exception.");
					data[i][j] = Double.NaN;
				}
			i++;
		}
		Class1 = classes[0];
		if(Class1.compareTo(classes[classes.length-1]) != 0)
			Class2 = classes[classes.length-1];
		else
			Class2 = classes[classes.length-2];
		for(i=0; i<classes.length; i++)
			if(classes[i].compareTo(Class1) == 0 )
				numberOfClass1++;
			else if(classes[i].compareTo(Class2) == 0)
				numberOfClass2++;
			else
				System.out.println("Unknown Class :"+classes[i]);
		if(DEBUGmode == 1){
			System.out.println("... Done.");
			System.out.println("Number of Instances in Class "+Class1 +" is :"+numberOfClass1);
			System.out.println("Number of Instances in Class "+Class2 +" is :"+numberOfClass2);
		}
	}
	*/

	
}
