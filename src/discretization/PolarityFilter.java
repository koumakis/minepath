package discretization;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import minepath.infoLogs;
import misc.FisherExact;
import misc.StdStats;
import misc.saveToFile;

public class PolarityFilter  extends FS{
	
	Vector<double[]> measures = new Vector();
	infoLogs inf;
	boolean relativeFiltering = true;
	//boolean truePosFiltering = true;
	boolean usePval = true;
	boolean useFoldChange = false;
	FisherExact fisher;
	double[] pvalues;

	/*
	public PolarityFilter(int DebugMode, saveToFile sf, String[] classes, boolean addRanking, infoLogs _inf, boolean _relativeFiltering, boolean _truePosFiltering) throws IOException{
		DEBUGmode = DebugMode;
		numberOfClass1 = 0;
		numberOfClass2 = 0;
		ranking = addRanking;
		inf = _inf;
		relativeFiltering = _relativeFiltering;
		truePosFiltering = _truePosFiltering;
		fisher = new FisherExact(classes.length +2);
		readData(sf, classes);
		pvalues = new double[simplepPredictor.length];
	}
	*/
	/*
	public PolarityFilter(int DebugMode, String inputFileName, boolean addRanking, infoLogs _inf, boolean _relativeFiltering, boolean _truePosFiltering) throws IOException{
		DEBUGmode = DebugMode;
		numberOfClass1 = 0;
		numberOfClass2 = 0;
		ranking = addRanking;
		inf = _inf;
		relativeFiltering = _relativeFiltering;
		truePosFiltering = _truePosFiltering;
		readData(inputFileName);
		pvalues = new double[simplepPredictor.length];
	}
	*/
	
	public PolarityFilter(int DebugMode, saveToFile sf, String[] classes, boolean addRanking, infoLogs _inf, boolean _usePval, boolean _useFoldChange) throws IOException {
		usePval = _usePval;
		useFoldChange = _useFoldChange;
		DEBUGmode = DebugMode;
		numberOfClass1 = 0;
		numberOfClass2 = 0;
		ranking = addRanking;
		inf = _inf;
		//relativeFiltering = true;
		//truePosFiltering = true;
		fisher = new FisherExact(classes.length +2);
		readData(sf, classes);
		pvalues = new double[simplepPredictor.length];
	}

	public void polarityMetric(String outputFileName, int orange, double threshold) throws IOException{

		
		//readData(inputFileName);

		double[][] tmp;
		for(int i=0; i<genes.length; i++){
			QuickSort q = new QuickSort();
			tmp = q.quicksort(setGeneAndClass(i));
			
			//genesRate[i] = caclulateBestInfoGainCut(tmp);
			//measures.add(i, polarityGeneRanking(i, genesRate[i], tmp));
			measures.add(i, polarityGeneRanking(i, tmp));
			
			//if(DEBUGmode == 1)
			//	System.out.println("Threshold for Gene "+genes[i] +" is :" +genesRate[i]);
		}

		
		//printAll(true);
		saveDiscreteFile(outputFileName);
		
		//saveNBestDiscreteFile(outputFileName.replaceAll("-all-pathways.txt", "-Best.txt"), orange, sd);
		saveBestPvaluesDiscreteFile(outputFileName.replaceAll("-all-pathways.txt", "-Best.txt"), orange, threshold);
	}
	
	public void polarityMetricForPermutation(Hashtable permutedSubpaths) throws IOException{
		for(int i=0; i<genes.length; i++){
//System.out.println(genes[i]);
			
			if(!permutedSubpaths.containsKey(genes[i]))
				continue;
			ArrayList[] values = (ArrayList[]) permutedSubpaths.get(genes[i]);

			QuickSort q = new QuickSort();
			double[] scores = polarityGeneRanking(i, q.quicksort(setGeneAndClass(i)));
			values[0].add(simplepPredictor[(int)scores[0]]); //score
			values[1].add(scores[5]); //p-value
		}

	}

	public void polarityMetricMiRNA(String inputFileName, String outputFileName, int orange, double threshold) throws IOException{
		readData(inputFileName);
		double[][] tmp;
		for(int i=0; i<genes.length; i++){
			QuickSort q = new QuickSort();
			tmp = q.quicksort(setGeneAndClass(i));
			measures.add(i, polarityGeneRanking(i, tmp));

		}	
		pvalues = new double[genes.length];
		saveDiscreteFile(outputFileName);	
		saveBestPvaluesDiscreteFile(outputFileName.replaceAll("-all-pathways.txt", "-Best.txt"), orange, threshold);
	}
		public void polarityMetric(saveToFile sf, String outputFileName, int _orange, double sd) {
		
	}
	
	//private double[] polarityGeneRanking(int index, double median,double[][] geneData){
	private double[] polarityGeneRanking(int index,double[][] geneData){
		int i,j;
		double[] ret = new double[6];
		
			int countClass1under=0, countClass1over=0, countClass2under=0, countClass2over = 0;
			for(i=0; i<geneData[0].length; i++){
				if(geneData[1][i] == 1) //Class 1
					if(geneData[0][i] == 0) //if(geneData[0][i] < median)
						countClass1under++;
					else
						countClass1over++;
				else
					if(geneData[0][i] == 0) //if(geneData[0][i] < median)
						countClass2under++;
					else
						countClass2over++;
			}
			if((countClass1over + countClass2over) ==0){
				simplepPredictor[index] = 0;
				//System.out.println("ZERO at index :"+index);
			}else{
				double relClass1over = (double)countClass1over/(double)(countClass1over + countClass1under);
				double relClass2over = (double)countClass2over/(double)(countClass2over + countClass2under);
				if(relativeFiltering)
					simplepPredictor[index] = (double)(relClass1over - relClass2over)/(double)(relClass1over + relClass2over);				
				else
					simplepPredictor[index] = (double)(countClass1over - countClass2over)/(double)(countClass1over + countClass2over);
/*
				//if(truePosFiltering){		//Boost the Ranking method to "promote" sub-paths which are more active (value 1) at the specific class 
					if(simplepPredictor[index]>0)
						simplepPredictor[index] = simplepPredictor[index] * relClass1over;
					else
						simplepPredictor[index] = simplepPredictor[index] * relClass2over;
				//}
*/				
			}
			ret[0] = index;				//sub-path id
			ret[1] = countClass1over; 	//Red at Class 1
			ret[2] = countClass1under; 	//Blue at Class1
			ret[3] = countClass2over; 	//Red at Class 2
			ret[4] = countClass2under; 	//Blue at Class2
			ret[5] = fisher.getTwoTailedP(countClass1over, countClass2over, countClass1under, countClass2under);				//p-value
			
			return ret;
	}
	
/*
	public void  saveNBestDiscreteFile(String fileName, int _orange, double sd) throws IOException{
        
		if(DEBUGmode == 1)
			System.out.print("Writing N best Features (Discretized data) to "+fileName +" ");
		
		double orangePersentage = (double)_orange/100;
		//double[] sorted = simplepPredictor.clone();
		//Arrays.sort(sorted);

		
		BufferedWriter writer = new BufferedWriter( new FileWriter(fileName) );
		//BufferedWriter writerPlus = new BufferedWriter( new FileWriter(fileName.substring(0, fileName.length()-12) +"PlusBest.txt") );
		//BufferedWriter writerMinus = new BufferedWriter( new FileWriter(fileName.substring(0, fileName.length()-12) +"MinusBest.txt") );
		BufferedWriter writerOrange = new BufferedWriter( new FileWriter(fileName.replaceAll("Best.txt", "OrangeBest.txt")));

		String lineToWrite = new String();
		
		lineToWrite = lineToWrite.concat("SubPathID\t");
		
		lineToWrite = lineToWrite.concat("Ranking\t");
		lineToWrite = lineToWrite.concat("P-Value\t");
		
		for(int i=0; i < classes.length;i++)
			lineToWrite = lineToWrite.concat(classes[i]+"\t");
		writer.write(lineToWrite);
		//writerPlus.write(lineToWrite);
		//writerMinus.write(lineToWrite);
		writerOrange.write(lineToWrite);
		
		lineToWrite = "";

		Vector orange = new Vector();			//The index of the sub-path (same index at ranking, data)
		Vector plus = new Vector();
		Vector minus = new Vector();
		
		for(int i=0; i<measures.size(); i++){
			double[] tmp = (double[])measures.get(i);
			double class1count = tmp[1] + tmp[2];
			double class2count = tmp[3] + tmp[4];
			double r1p = (class1count == 0) ? 0 : tmp[1]/class1count;
			double r2p = (class2count == 0) ? 0 : tmp[3]/class2count;
			if(r1p-r2p > 0)
					pvalues[i] = tmp[5];
			else
					pvalues[i] = -1*tmp[5];
			if( (r1p >= orangePersentage) && (r2p >= orangePersentage) ){
				orange.add( (new Double(tmp[0])).intValue() );
			}else if( (r1p-r2p > 0) && (simplepPredictor[i]>0) )
				plus.add(new Double(simplepPredictor[i]));
			else if( (r1p-r2p < 0) && (simplepPredictor[i]<0) )
				minus.add(new Double(simplepPredictor[i]));
		}
		double[] tmpPlus = new double[plus.size()];
		for(int i=0; i<plus.size(); i++)
			tmpPlus[i] = ((Double)plus.get(i)).doubleValue();
		inf.class1Mean =  (plus.size() == 0) ? 0.0001 : StdStats.mean(tmpPlus);
		inf.class1Std =   (plus.size() == 0) ? 0.0001 : StdStats.stddev(tmpPlus);

		double[] tmpMinus = new double[minus.size()];
		for(int i=0; i<minus.size(); i++)
			tmpMinus[i] = ((Double)minus.get(i)).doubleValue();
		inf.class2Mean =  (minus.size() == 0) ? 0.0001 : StdStats.mean(tmpMinus);
		inf.class2Std =   (minus.size() == 0) ? 0.0001 : StdStats.stddev(tmpMinus);
		
		inf.class1Threshold = inf.class1Mean  + sd*inf.class1Std;
		inf.class2Threshold = inf.class2Mean - sd*inf.class2Std;
		System.out.println("\n\n=========================" +
				"\nPositive Class.\nMean\t\t"+String.format(Locale.US, "%.20f", inf.class1Mean)+"\nStd\t\t"+String.format(Locale.US, "%.20f", inf.class1Std)+"\nThreshold\t" +inf.class1Threshold);
		System.out.println("\nNegative Class.\nMean\t\t"+String.format(Locale.US, "%.20f", inf.class2Mean)+"\nStd\t\t"+String.format(Locale.US, "%.20f", inf.class2Std)+"\nThreshold\t" +inf.class2Threshold 
				+"\n=========================\n");
		
		inf.orangeThreshold = orangePersentage;
		
		plus = null;
		minus = null;

		for(int i=0; i<genes.length; i++){
				//System.out.println(thrPos +"\t" +thrNeg +"\t" +simplepPredictor[i] );
				//inf.addPathEdgesWithScore(genes[i], simplepPredictor[i], false);
				
				
				//if( (simplepPredictor[i] > inf.class2Threshold ) && (simplepPredictor[i] < inf.class1Threshold) ) //Filter 1 if is less than the average ignore
				//	continue;
				if( Math.abs(pvalues[i]) >= 0.01)
					continue;
				
				
				//If you want to ignore p-values call
				// inf.addPathEdgesWithScore(genes[i], simplepPredictor[i], Double.NaN, false);
				inf.addPathEdgesWithScore(genes[i], simplepPredictor[i], pvalues[i], "", false);
				
				//System.out.println("Passed !");
				lineToWrite = lineToWrite.concat("\n" +genes[i] +"\t");			
				lineToWrite = lineToWrite.concat(simplepPredictor[i] +"\t");	

				lineToWrite = lineToWrite.concat( String.format(Locale.US, "%.20f", pvalues[i]) +"\t");	
				
				for(int j=0; j<data[i].length; j++){
					if(data[i][j] > genesRate[i])
						lineToWrite = lineToWrite.concat("1\t");
					else
						lineToWrite = lineToWrite.concat("0\t");
				}
				writer.write(lineToWrite);
				
				//if(simplepPredictor[i] >= thrPos)
				//	writerPlus.write(lineToWrite);
				//else
				//	writerMinus.write(lineToWrite);
				
				lineToWrite = "";
		}
		
		
		// Add functional sub paths for both classes (Orange color coding) into both (plus - minus) best... 
		for(int k=0; k<orange.size(); k++){
			int i = (Integer) orange.get(k) ;
			lineToWrite = lineToWrite.concat("\n" +genes[i] +"\t");			
			lineToWrite = lineToWrite.concat(simplepPredictor[i] +"\t");	
			int tempcount = 0;
			for(int j=0; j<data[i].length; j++){
				if(data[i][j] > genesRate[i]){
					lineToWrite = lineToWrite.concat("1\t");
					tempcount++;
				}else{
					lineToWrite = lineToWrite.concat("0\t");
				}
			}
			//Write it to plus AND to minus best to get the appropriate (orange, grey) color...
			//writerPlus.write(lineToWrite);
			//writerMinus.write(lineToWrite);
			writerOrange.write(lineToWrite);
			lineToWrite = "";
			
			//If you want to ignore p-values call
			// inf.addPathEdgesWithScore(genes[i], simplepPredictor[i], Double.NaN, false);
			inf.addPathEdgesWithScore(genes[i], (double)tempcount/data[i].length, pvalues[i], "", true);
		}
		//System.out.println("Orange size: " +orange.size() +"\t" +inf.subPathsOrange.size());
		writer.flush();
		writer.close();
		
		//writerPlus.flush();
		//writerPlus.close();
		//writerMinus.flush();
		//writerMinus.close();
		
		writerOrange.flush();
		writerOrange.close();
		if(DEBUGmode == 1)
			System.out.println("... Done.");

	}
	*/
	
	public void  saveBestPvaluesDiscreteFile(String fileName, int _orange, double thr) throws IOException{
        
		if(DEBUGmode == 1)
			System.out.print("Writing N best Features (Discretized data) to "+fileName +" ");
		
		double orangePersentage = (double)_orange/100;
		//double[] sorted = simplepPredictor.clone();
		//Arrays.sort(sorted);

		
		BufferedWriter writer = new BufferedWriter( new FileWriter(fileName) );
		BufferedWriter writerOrange = new BufferedWriter( new FileWriter(fileName.replaceAll("Best.txt", "OrangeBest.txt")));

		String lineToWrite = new String();
		
		lineToWrite = lineToWrite.concat("SubPathID\t");
		
		lineToWrite = lineToWrite.concat("Ranking\t");
		lineToWrite = lineToWrite.concat("P-Value\t");
		
		for(int i=0; i < classes.length;i++)
			lineToWrite = lineToWrite.concat(classes[i]+"\t");
		writer.write(lineToWrite);
		writerOrange.write(lineToWrite);
		
		lineToWrite = "";

		Vector orange = new Vector();			//The index of the sub-path (same index at ranking, data)
		Vector plus = new Vector();
		Vector minus = new Vector();
		
		for(int i=0; i<measures.size(); i++){
			double[] tmp = (double[])measures.get(i);
			double class1count = tmp[1] + tmp[2];
			double class2count = tmp[3] + tmp[4];
			double r1p = (class1count == 0) ? 0 : tmp[1]/class1count;
			double r2p = (class2count == 0) ? 0 : tmp[3]/class2count;
			if(r1p-r2p > 0)
					pvalues[i] = tmp[5];
			else
					pvalues[i] = -1*tmp[5];
			if( (r1p >= orangePersentage) && (r2p >= orangePersentage) ){
				orange.add( (new Double(tmp[0])).intValue() );
			}else if( (r1p-r2p > 0) && (simplepPredictor[i]>0) )
				plus.add(new Double(simplepPredictor[i]));
			else if( (r1p-r2p < 0) && (simplepPredictor[i]<0) )
				minus.add(new Double(simplepPredictor[i]));
		
		}
		double[] tmpPlus = new double[plus.size()];
		for(int i=0; i<plus.size(); i++)
			tmpPlus[i] = ((Double)plus.get(i)).doubleValue();
		inf.class1Mean =  (plus.size() == 0) ? 0.0001 : StdStats.mean(tmpPlus);
		inf.class1Std =   (plus.size() == 0) ? 0.0001 : StdStats.stddev(tmpPlus);

		double[] tmpMinus = new double[minus.size()];
		for(int i=0; i<minus.size(); i++)
			tmpMinus[i] = ((Double)minus.get(i)).doubleValue();
		inf.class2Mean =  (minus.size() == 0) ? 0.0001 : StdStats.mean(tmpMinus);
		inf.class2Std =   (minus.size() == 0) ? 0.0001 : StdStats.stddev(tmpMinus);
		
		//inf.class1Threshold = inf.class1Mean  + thr*inf.class1Std;
		//inf.class2Threshold = inf.class2Mean - thr*inf.class2Std;
		
		inf.class1Threshold = thr;
		inf.class2Threshold = -1*thr;
		System.out.println("\n\n=========================" +
				"\nPositive Class.\nMean\t\t"+inf.class1Mean+"\nStd\t\t"+inf.class1Std+"\nThreshold\t" +inf.class1Threshold);
		System.out.println("\nNegative Class.\nMean\t\t"+inf.class2Mean+"\nStd\t\t"+inf.class2Std+"\nThreshold\t" +inf.class2Threshold 
				+"\n=========================\n");
		
		inf.CommonThreshold = orangePersentage;
		
		plus = null;
		minus = null;
		int polarityCounts =0, pcounts = 0, fcounts =0, ccounts = 0;
		for(int i=0; i<genes.length; i++){
				//System.out.println(thrPos +"\t" +thrNeg +"\t" +simplepPredictor[i] );
				//inf.addPathEdgesWithScore(genes[i], simplepPredictor[i], false);
				
				
				if( (simplepPredictor[i] > inf.class2Threshold ) && (simplepPredictor[i] < inf.class1Threshold) ) //Filter 1 if is less than the average ignore
					continue;
			
				double[] tmp = (double[])measures.get(i);
				String coverage = "coverage:[("+(int)tmp[1] +" out of "+((int)tmp[2]+(int)tmp[1]) +") , (" +(int)tmp[3] +" out of "+((int)tmp[3]+(int)tmp[4]) +")]";
				polarityCounts++;
				
				if(usePval && (Math.abs(pvalues[i]) >= 0.05) )
					continue;
				pcounts++;
				
				//At least 2 fold change diff filter
				if(useFoldChange){
					double foldChange;
					if(tmp[3] == 0)
						foldChange = tmp[1];
					else if(tmp[1] == 0)
						foldChange = tmp[3];
					else
						foldChange = ( tmp[1]/(tmp[2]+tmp[1]) )/( tmp[3]/(tmp[4]+tmp[3]));
					
					if( (foldChange > 0.5) && (foldChange < 2) )
						continue;
					fcounts++;
				}
				//At least 25% coverage for the specific class 
				if( (pvalues[i] >= 0) && (tmp[1]/(tmp[2]+tmp[1]) < 0.25) )
					continue;
				if( (pvalues[i] < 0) && (tmp[3]/(tmp[4]+tmp[3]) < 0.25) )
					continue;
				/* //We don't need it. If the score thr is 0.5 then subpaths with more than 0.33% at the opposite class does not pass the threshold.
				if( (pvalues[i] >= 0) && (tmp[3]/(tmp[4]+tmp[3]) > 0.25) )
					continue;
				if( (pvalues[i] < 0) &&  (tmp[1]/(tmp[2]+tmp[1])  > 0.25) )
					continue;
				*/
				ccounts++;
				
//System.out.println(genes[i]+"\t" +simplepPredictor[i] +"\t" +pvalues[i] +"\t" +tmp[1] +"\t" +tmp[2] +"\t" +tmp[3] +"\t" +tmp[4] +"\t" +tmp[5]);
				//If you want to ignore p-values call
				// inf.addPathEdgesWithScore(genes[i], simplepPredictor[i], Double.NaN, false);
				inf.addPathEdgesWithScore(genes[i], simplepPredictor[i], pvalues[i], coverage, false);
				
				//System.out.println("Passed !");
				lineToWrite = lineToWrite.concat("\n" +genes[i] +"\t");			
				lineToWrite = lineToWrite.concat(simplepPredictor[i] +"\t");	

				lineToWrite = lineToWrite.concat( String.format(Locale.US, "%.20f", pvalues[i]) +"\t");	
				
				for(int j=0; j<data[i].length; j++){
					if(data[i][j] > genesRate[i])
						lineToWrite = lineToWrite.concat("1\t");
					else
						lineToWrite = lineToWrite.concat("0\t");
				}
				writer.write(lineToWrite);

				lineToWrite = "";
		}
//System.out.println("Passed polarity thr:" +polarityCounts +"\tplus p-values:"+pcounts +"\tplus fold counts:"+fcounts +"\tplus coverage:"+ccounts);		
		
		// Add functional sub paths for both classes (Orange color coding) into both (plus - minus) best... 
		for(int k=0; k<orange.size(); k++){
			int i = (Integer) orange.get(k) ;
			lineToWrite = lineToWrite.concat("\n" +genes[i] +"\t");			
			lineToWrite = lineToWrite.concat(simplepPredictor[i] +"\t");	
			int tempcount = 0;
			for(int j=0; j<data[i].length; j++){
				if(data[i][j] > genesRate[i]){
					lineToWrite = lineToWrite.concat("1\t");
					tempcount++;
				}else{
					lineToWrite = lineToWrite.concat("0\t");
				}
			}
			//Write it to plus AND to minus best to get the appropriate (orange, grey) color...
			//writerPlus.write(lineToWrite);
			//writerMinus.write(lineToWrite);
			writerOrange.write(lineToWrite);
			lineToWrite = "";
			
			double[] tmp = (double[])measures.get(i);
			String coverage = "coverage:[("+(int)tmp[1] +" out of "+((int)tmp[2]+(int)tmp[1]) +") , (" +(int)tmp[3] +" out of "+((int)tmp[3]+(int)tmp[4]) +")]";
//System.out.println(genes[i] +"\t" +(double)tempcount/data[i].length +"\t" +pvalues[i] +"\t" +coverage);

			inf.addPathEdgesWithScore(genes[i], (double)tempcount/data[i].length, pvalues[i], coverage, true);
		}
		//System.out.println("Orange size: " +orange.size() +"\t" +inf.subPathsOrange.size());
		writer.flush();
		writer.close();

		writerOrange.flush();
		writerOrange.close();
		if(DEBUGmode == 1)
			System.out.println("... Done.");

	}

}
