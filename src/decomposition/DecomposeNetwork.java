package decomposition;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import minepath.infoLogs;

/*
 * EdgeView coding from KEGG parser:
 * 3  Activation
 * 15 Inhibition
 * 9  Dissociation
 * 0  Binding/Association
 * 
 * 
 * To add/ignore binding & dissociation line 122 at dfs
 * To add/ignore the probeset id in the path name go to line 282 and enable/disable setPaths(genes, path); or setPathsWithProbes(genes, path); accordingly
 */

public class DecomposeNetwork {
	private static final long serialVersionUID = 55L;
	
	static int DEBUGmode =0;
	static boolean ignoreInverseInhibition = false;
	
	public Hashtable listOfPaths = new Hashtable();
	public Hashtable listOfPathsWithDublicates = new Hashtable();
	public List<String> nonFunctionalSubPaths = new ArrayList<String>();  //Sub-paths with no active state in any Sample
	public infoLogs info;
	public String pathwayName;
	String pathwayPath;
	public List<String> listNode = null;
	public String title;
	public int edgeCount;
	
	
	public DecomposeNetwork(int DebugLevel, boolean _ignoreInverseInhibition, infoLogs inf, String _pathwayPath, String _pathway, String seralizedNetworkFile){
		DEBUGmode = DebugLevel;		
		ignoreInverseInhibition = _ignoreInverseInhibition;
		info = inf;
		pathwayName = _pathway;
		deserializeNetwork(seralizedNetworkFile);
	}
	  
	public void serializeNetwork(){
		NetworkSerializableData n = new NetworkSerializableData(pathwayName, pathwayPath, title, edgeCount, listNode, listOfPaths, listOfPathsWithDublicates, info.getPath2edge());
		try {
			n.serializeThis();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void serializeNetwork(String path){
		NetworkSerializableData n = new NetworkSerializableData(pathwayName, pathwayPath, title, edgeCount, listNode, listOfPaths, listOfPathsWithDublicates, info.getPath2edge());
		try {
			n.serializeThis(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deserializeNetwork(String seralizedNetworkFile){
		NetworkSerializableData n = null;
		FileInputStream fis;
		try {
			fis = new FileInputStream(seralizedNetworkFile);
			  BufferedInputStream bis = new BufferedInputStream(fis);
			  ObjectInputStream ois = new ObjectInputStream(bis);
			  n = (NetworkSerializableData) ois.readObject();
			  ois.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pathwayName = n.pathwayName;
		pathwayPath = n.pathwayPath;
		title = n.title;
		edgeCount = n.edgeCount;
		listNode = n.listNode;
		listOfPaths = n.listOfPaths;
		listOfPathsWithDublicates = n.listOfPathsWithDublicates;
		
		info.addPathInfo(pathwayName, 0, edgeCount);
		info.setPath2edge(n.path2edge);
	}

	public void destroy(){
		listNode = null;
		listOfPaths = null;
		listOfPathsWithDublicates = null;
		nonFunctionalSubPaths = null;
	}
	

	
	public void addPath(Hashtable h, Vector v){
		Vector thepath = new Vector(v);
		String key = stingVectorToSting(v);
		if (h.containsKey(key)) //Path already exists
			return;

		h.put(key, thepath);
	}
	
	private String stingVectorToSting(Vector v){
		String ret = new String();
		String ss;
		String edgeIds = new String();
		for(int i=0; i<v.size(); i++){
			ss = (String)v.get(i);
			if(ss.compareTo("3") == 0)
				ret = ret.concat("-->");
			else if(ss.compareTo("15") == 0)
				ret = ret.concat("--|");
			else if(ss.compareTo("0") == 0)
				ret = ret.concat("<-->");
			else if(ss.compareTo("9") == 0)
				ret = ret.concat("-//-");
			else {
				
				if(ss.indexOf("!") > 0){
					edgeIds = edgeIds +"," +ss.split("!")[1];
					ret = ret.concat(ss.substring(0, ss.indexOf("!")));
				}else
				
					ret = ret.concat(ss);
			}
		}
		
		//System.out.println(ret);
		return ret;
	}
	

	public void enrichSubpathsWithDublicateGenes(Hashtable genes){
		System.out.println("Number of sub-paths before dublicates :"+listOfPaths.size());
		Hashtable tmp = new Hashtable();
		Set set = listOfPaths.entrySet(); 
		// Get an iterator 
		Iterator i = set.iterator(); 
		// Display elements 
		int counter=0;
		while(i.hasNext()) { 
			
			Map.Entry me = (Map.Entry)i.next(); 
			Vector path = (Vector)me.getValue();
			//System.out.println(counter++ +"\t"+stingVectorToSting(path));
			createDuplicates(genes, path);
		}
		System.out.println("Number of sub-paths after dublicates :"+listOfPathsWithDublicates.size());
		listOfPaths = null;
		listOfPaths = listOfPathsWithDublicates;
	}
	
	
	private void createDuplicates(Hashtable genes, Vector path){
		//System.out.println("Checking :" +stingVectorToSting(path));
		boolean noHit = true;
		
		String ss;
		for(int i=0; i<path.size(); i++){
			ss = (String)path.get(i);
			 //If we don't want to ignore the association disassosiation
			  	if( (ss.compareTo("3") == 0) || (ss.compareTo("15") == 0) || (ss.compareTo("0") == 0) || (ss.compareTo("9") == 0) )
				continue;
			String[] split = ss.split(" ");
			if(split.length >1){
				String allInOne = new String();
				for(int j=0; j< split.length; j++){
					if(genes.containsKey(split[j])){
						Vector dGenes = (Vector)genes.get(split[j]);
						noHit = false;
						for(int w=0; w<dGenes.size(); w++)
							allInOne = allInOne.concat((String)dGenes.get(w) +" ");
					}
				}
				if(noHit == false){
					path.set(i,  allInOne);
					createDuplicates(genes, path);
					return;
				}
				
			}else if(genes.containsKey(ss)){
				Vector dGenes = (Vector)genes.get(ss);
				noHit = false;
				Vector dublicate = (Vector) path.clone();
				for(int w=0; w<dGenes.size(); w++){							
					dublicate.set(i, (String)dGenes.get(w));
					createDuplicates(genes, dublicate);
				}
				return;
			}
		}
		if(noHit){
			addPath(listOfPathsWithDublicates, path);
			//System.out.println(stingVectorToSting(path));
		}
	}
	
	
	public void enrichSubpathsWithDublicateGenesAsOR(Hashtable genes){
		
		Hashtable tmp = new Hashtable();
		Set set = listOfPaths.entrySet(); 
		Iterator i = set.iterator(); 
		int counter=0;
		while(i.hasNext()) { 
			
			Map.Entry me = (Map.Entry)i.next(); 
			Vector path = (Vector)me.getValue();
			setPathsWithProbes(genes, path);
		}

	}

	private void setPathsWithProbes(Hashtable genes, Vector path){
		//System.out.println("Checking	:" +stingVectorToSting(path));
		boolean noHit = true;
		String ss;
		for(int i=0; i<path.size(); i++){
			ss = (String)path.get(i);
			  	if( (ss.compareTo("3") == 0) || (ss.compareTo("15") == 0) || (ss.compareTo("0") == 0) || (ss.compareTo("9") == 0) )
				continue;

			String[] split = ss.split(" ");
			if(split.length >1){
				String allInOne = new String();
				for(int j=0; j< split.length; j++){

					if(genes.containsKey(split[j])){
						Vector dGenes = (Vector)genes.get(split[j]);

						noHit = false;
						for(int w=0; w<dGenes.size(); w++)
							allInOne = allInOne.concat((String)dGenes.get(w) +" ");
					}else
						allInOne = allInOne.concat("noProbe#"+split[j] +" ");
				}
				if(noHit == false){
					path.set(i,  allInOne);
				}
				
			}else if(genes.containsKey(ss)){
				String tmpStr = new String();
				Vector dGenes = (Vector)genes.get(ss);
				noHit = false;
				for(int w=0; w<dGenes.size(); w++){	
					tmpStr = tmpStr.concat((String)dGenes.get(w) +" ");					
				}
				tmpStr = tmpStr.substring(0, tmpStr.length()-1);
				path.set(i, tmpStr);

			}
		}
	}
	
	public void addStartEndSubpath() {
		Hashtable tmp = new Hashtable();
		tmp = (Hashtable) listOfPaths.clone();

		Set set = listOfPaths.entrySet(); 
		// Get an iterator 
		Iterator i = set.iterator(); 
		// Display elements 
		int counter=0;
		Vector v = null;
		System.out.println("Number of sub-paths before adding start-end 	:"+listOfPaths.size());		
		while(i.hasNext()) { 			
			Map.Entry me = (Map.Entry)i.next(); 
			Vector path = (Vector)me.getValue();
			if(path.size() < 4) //Not only 2 genes with one interaction
				continue;
			v = new Vector();
			boolean isActivation = true;
			for (int w=0; w<path.size(); w++)
				if( ((String)path.get(w)).compareTo("15") == 0 )
					isActivation = !isActivation;
			
			if(isActivation){
				v.add(path.get(0));
				v.add("3"); //Activation
				v.add(path.get(path.size()-1));
				addPath(tmp, v);
			}else{
				v = new Vector();
				v.add(path.get(0));
				v.add("15"); //Inhibition
				v.add(path.get(path.size()-1));
				addPath(tmp, v);
			}
		}
		System.out.println("Number of sub-paths after start-end 		:"+tmp.size());
		listOfPaths = null;
		listOfPaths = tmp;
		tmp = null;
	}
	
	public Hashtable pathsWithMAvalues( Hashtable MAdata){
		Hashtable ret = new Hashtable();
		BitSet curBitSet = null;
		Set set = listOfPaths.entrySet(); 
		Iterator i = set.iterator(); 
		while(i.hasNext()) { 
			Map.Entry me = (Map.Entry)i.next(); 
			curBitSet = getMAdataForPath( (Vector)me.getValue(), MAdata); 
			
			if (curBitSet == null){ //Ignore that Path. Contains none true value in the MAdataset
						
				nonFunctionalSubPaths.add(stingVectorToSting( (Vector)me.getValue() ));
				continue;
			}
			ret.put(stingVectorToSting( (Vector)me.getValue() ), curBitSet);
		} 
		if(ret.size() == 0)
			return null;
		return ret;
	}
	
	private BitSet getMAdataForPath(Vector path, Hashtable MAdata){
		BitSet newReaction 			= null;
		BitSet ancestorReactions   	= null;
		String s;
		String curAssociation = new String();
		for(int i=0; i<path.size(); i++){
			s = (String)path.get(i);
			if(i==0){
				BitSet tmpBS = getBitsetFromNode(s, MAdata);
				if(tmpBS ==null)
					return null;
				newReaction 		= (BitSet) (tmpBS).clone();
				ancestorReactions   = (BitSet) (tmpBS).clone();
				continue;
			}
			if ( (s.compareTo("3") == 0) || (s.compareTo("15") == 0) || (s.compareTo("9") == 0) || (s.compareTo("0") == 0) ){
				curAssociation = s;
				continue;
			}
			//Check if we have more than one genes in the current node and performs a logical OR on the MA data of them.
			BitSet targetGene = getBitsetFromNode(s, MAdata);	
			if(targetGene ==null)
				return null;
			
			if(newReaction.size() != targetGene.size()){
				printBitSet(newReaction);
				printBitSet(targetGene);
				System.out.println("" +newReaction.size() +"\t"+ targetGene.size() );
			}
			if(curAssociation.compareTo("15") == 0){ //Inhibition	Valid Up--|Down OR Down--|Up
				if(ignoreInverseInhibition)
					newReaction.andNot(targetGene ); //Up--|Down ONLY
				else
				newReaction.xor(targetGene);
			}else if(curAssociation.compareTo("9") == 0){ //DisAssociation
				//All the combinations hold. It is not a regulatory mechanism it is a physical association
			}else if(curAssociation.compareTo("0") == 0){ //Binding - association
				//All the combinations hold. It is not a regulatory mechanism it is a physical association
			}else{ //Activation			Only Up --> Up is considered to be valid
				newReaction.and( targetGene );
			}
			if(i>3){ // More than 1 reactions
				newReaction.and( ancestorReactions );
			}
			ancestorReactions 	= (BitSet) newReaction.clone();
			newReaction 		= (BitSet) targetGene.clone();    //Get the last gene (target) as the new source for the next reaction
		}
		if ( (ancestorReactions.isEmpty()) ) // Returns true if this BitSet contains no bits that are set to true.
			return null;
		else
			return ancestorReactions;
	}
	
	private BitSet getBitsetFromNode(String s, Hashtable MAdata){
		//Check if we have more than one genes in the current node and performs a logical OR on the MA data of them.
		BitSet currentBS = null;
		if(s.contains(" ")){
			String[] sTable = s.split(" ");
			int w=0;
			//Find the first and initialize a tmp BitSet
			for (w=0; w < sTable.length; w ++){
				BitSet tmpbs = (BitSet) MAdata.get(sTable[0]);
				if (tmpbs !=null){
					currentBS = (BitSet) (tmpbs).clone(); //Initialize. Get the first and then add the rest
					//Compute a logical on OR at the previous BitSet
					for ( ; w < sTable.length; w ++){
						if( MAdata.get(sTable[w]) != null)
							currentBS.or((BitSet) MAdata.get(sTable[w]));
					}
				}
			}
		}
		else
			currentBS = (BitSet) MAdata.get(s);
		return currentBS;
	}
	
	public void printAllSubPaths() throws IOException{
		BufferedWriter writer = new BufferedWriter(new FileWriter("allsubpaths.txt"));
		System.out.println("--------------------");
		System.out.println("Printing all the sub-paths");
		Set set = listOfPaths.entrySet(); 
		// Get an iterator 
		Iterator i = set.iterator(); 
		// Display elements 
		while(i.hasNext()) { 
			Map.Entry me = (Map.Entry)i.next(); 
			System.out.println((String)me.getKey() +"\t"+stingVectorToSting((Vector)me.getValue()) +"\tRelations:" +((int)((Vector)me.getValue()).size()/2) );	
			writer.write((String)me.getKey()+"\n");
		}
		System.out.println("--------------------");
		writer.flush();
		writer.close();
		
	}
	
	public void printMAdata(Hashtable MAdata){
		Set set = MAdata.entrySet(); 
		Iterator i = set.iterator(); 
		//int j=0;
		while(i.hasNext()) { 
			Map.Entry me = (Map.Entry)i.next(); 
			System.out.print( (String)me.getKey() +"\t");
			printBitSet( (BitSet)me.getValue());
			//j++;
			//if (j>100) return;
		}
		
	}
	
	private void printBitSet(BitSet b){
		for(int i=0; i<b.size(); i++)
			if ( b.get(i) )
				System.out.print("1 ");
			else
				System.out.print("0 ");
		System.out.println();
					
				
	}
}

class NetworkSerializableData implements Serializable{
	//private static final long serialVersionUID = 1L;
	
	String pathwayName;
	String pathwayPath;
	String title;
	int edgeCount;
	List<String> listNode = null;
	Hashtable listOfPaths = new Hashtable();
	Hashtable listOfPathsWithDublicates = new Hashtable();
	Hashtable path2edge;
	
	public NetworkSerializableData(String _pathwayName, String _pathwayPath, String _title, int _edgeCount, List<String> _listNode, Hashtable _listOfPaths, Hashtable _listOfPathsWithDublicates, Hashtable _path2edge){
		pathwayName = _pathwayName;
		pathwayPath = _pathwayPath;
		title = _title;
		edgeCount = _edgeCount;
		listNode = _listNode;
		listOfPaths = _listOfPaths;
		listOfPathsWithDublicates = _listOfPathsWithDublicates;
		path2edge = _path2edge;
	}
	
	public void serializeThis() throws IOException {
        FileOutputStream fos = new FileOutputStream(pathwayName +".dn");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(this);
        oos.close();
    }
	
	public void serializeThis(String path) throws IOException {
        FileOutputStream fos = new FileOutputStream(path +"/" +pathwayName +".dn");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(this);
        oos.close();
    }
}
