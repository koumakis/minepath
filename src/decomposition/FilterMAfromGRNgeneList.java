package decomposition;

import java.util.BitSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

public class FilterMAfromGRNgeneList {
	
	static int DEBUGmode = 0;
	public String[] genesInPathway = null;

	public FilterMAfromGRNgeneList(int DebugLevel){
		DEBUGmode = DebugLevel;
	}
	
	/**
	 * Filter the Microarray dataset according to the network list nodes. Eliminates genes 
	 * from microarray that are not in the Node list of GRN.
	 * @param genes The list of all genes in Microarray (already annotated)
	 * @param dataSet The dataset discretized
	 * @param listNode The list of nodes appeared in the GRN.
	 */
	public Hashtable filter(String[] genes, boolean[][] dataSet, List listNode){
		Hashtable MAdata = new Hashtable(listNode.size());
		genesInPathway = new String[listNode.size()];
		listNode.toArray(genesInPathway);
		
		for(int i=0; i<genes.length; i++){
			for(int k=0; k<genesInPathway.length; k++)
				if (genes[i].endsWith(genesInPathway[k])){
					BitSet curBitSet = new BitSet(dataSet[i].length);
					for(int j=0; j<dataSet[i].length; j++)
						curBitSet.set(j, dataSet[i][j]);
					MAdata.put(genes[i], curBitSet);
			}
		}
		return MAdata;
	}


	public Hashtable groupDublicateNames(String[] genes){

		Hashtable groupedGenes = new Hashtable();
		for (int i=0; i<genes.length; i++){
			String[] split = genes[i].split("#");
			if( (split.length > 1) && (groupedGenes.containsKey(split[1]) ) ){
				Vector v = (Vector)groupedGenes.get(split[1]);
				v.add(genes[i]);
			}else if(split.length == 1)
				continue;
			else{
				Vector<String> v = new Vector();
				v.add(genes[i]);
				groupedGenes.put(split[1], v);
			}
		}
		return groupedGenes;
	}

}
