package minepath;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import misc.saveToFile;
import misc.validation;
import decomposition.DecomposeNetwork;
import decomposition.FilterMAfromGRNgeneList;
import discretization.DiscrEntropyBased;
import discretization.DiscrKmeans;
import discretization.DiscrMeanPerGene;
import discretization.FS;
import discretization.PolarityFilter;


public class run {

	static int DEBUGmode = 0;
	static boolean cloneSubpaths = false;
	static boolean addStartEndSubpath = false; //e.g. a->b->c then add the a->c subpath (Kafetzopoulos request - b can be disturbed gene)
	static boolean ignoreSmallPaths = false; // Paths with only 2 genes
	static boolean ignoreInverseInhibition = false; // Use or not the inverse inhibition e.g. Down --| Up
	public String summary = new String();

	//static String dataPath = "C:/Lefteris/PhD/Data/Test1-OnlyUntreated-ClassER/";
	//static String pathwayDatPath ="C:/Lefteris/PhD/Data/Pathways/xgmml/";

	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		
		
		if(args.length >=2){
			int findSlash = args[0].lastIndexOf("/");
			String MAdataFile = args[0].substring(findSlash+1, args[0].length()); 
			String dataPath = args[0].substring(0, findSlash+1);
			String keggPathways = args[1];
			int _orange = 90;
			double _threshold = 0.5;
			int classifierId = 3;
			boolean usePval = true;
			boolean useFoldChange = false;
			int discrMethod = 0; //Default discretization method is Entropy based
			
			run r = new run();
			
			for(int j=2; j<args.length; j++){
				if(args[j].compareTo("-kmeans") == 0)
					discrMethod = 1;
				else if(args[j].compareTo("-mean") == 0)
					discrMethod = 2;
				else if(args[j].compareTo("-ignoreInverseInhibition") == 0 )
					ignoreInverseInhibition = true;
				else if(args[j].compareTo("-usePval") == 0 )
					usePval = true;
				else if(args[j].compareTo("-useFoldChange") == 0 )
					useFoldChange = true;
				else if(args[j].compareTo("-upBoth") == 0){
					try{
						_orange = (new Integer(args[j+1])).intValue();
						j++;
					}catch (Exception e){
						System.err.println("Error: \nUnrecognized argument :"+args[j+1]);
						System.out.println("\n");
						myerror();
					}
				}else if(args[j].compareTo("-Threshold") == 0){
					try{
						
						NumberFormat format = NumberFormat.getInstance(Locale.US);
						Number number = format.parse(args[j+1]);
						_threshold = number.doubleValue();
						j++;
					}catch (Exception e){
						System.err.println("Error: \nUnrecognized argument :"+args[j+1]);
						System.out.println("\n");
						myerror();
					}
				}else if(args[j].compareTo("-classifier") == 0){
					try{
						classifierId= (new Integer(args[j+1])).intValue();
						j++;
					}catch (Exception e){
						System.err.println("Error: \nUnrecognized argument :"+args[j+1]);
						System.out.println("\n");
						myerror();
					}
				}
			}
				r.runPathMine(MAdataFile, dataPath, keggPathways, discrMethod, _orange, _threshold, usePval, useFoldChange, classifierId, "kegg-titles.txt");

		}else{
			myerror();
		}
	}

	
	private static void myerror(){
		System.err.println("Usage for MinePath:\nMinePath <MicroArray FileName full path> <Pathways Folder Path> " +
				"\n\t\t Optional (parameters must appear after microarray & pathways paths):" +		
				"\n\t\t\t-kmeans set K-means as discretization method (default Entropy based)" +
				"\n\t\t\t-mean   set Mean    as discretization method (default Entropy based)" +
				"\n\t\t\t-upBoth N (% selected up regulated sub-paths for both classes) 	Default 90" +
				"\n\t\t\t-Threshold (value between 0-1) 	Default 0.5" +
				"\n\t\t\t-usePval (Set p-value <= 0.05 as threshold alogn with polarity for each subpath) 		Default true " +
				"\n\t\t\t-useFoldChange (Set Fold Change >= 2 as threshold alogn with polarity for each subpath) 		Default false " +
				"\n\t\t\t-ignoreInverseInhibition (ignore inverse inhibition Down --| Up) Default false (use it)" +
				"\n\t\t\t#-classifier (10 fold cross validation of best subpaths) 				1 = C4.5 decision tree , 2 NaiveBayes, 3 Support Vector Machines (Default), 0 none\n"); 
		System.exit(0);
	}
	
	public void runPathMine(String MAdataFile, String dataPath, String pathwayDataPath, int discrMethod, int _orange, double threshold, boolean usePval, boolean useFoldChange, int classifierId, String keggTitles) throws IOException{

		infoLogs inf = new infoLogs(dataPath, keggTitles);
		inf.MAfileName = MAdataFile;
		summary = summary.concat("Microarray file name:" +MAdataFile);

		FS discr;
		if(discrMethod == 2)
			discr = new DiscrMeanPerGene(DEBUGmode, false);
		else if (discrMethod == 1)
			discr = new DiscrKmeans(DEBUGmode, false);
		else  //The default method
			discr = new DiscrEntropyBased(DEBUGmode, false);
		
		discr.startMetric(dataPath + MAdataFile);
		boolean[][] discrData = discr.getDiscritezedData();
				
		inf.class1Name = discr.Class1;
		inf.class2Name = discr.Class2;
		summary = summary.concat("\nClasses in Microarray\nClass 1:" +discr.Class1 +"\nClass 2:" +discr.Class2 +"\n");
		
		new File(dataPath +"results/").mkdir();
		new File(dataPath +"results/perPathway/").mkdir();
		
		File dir = new File(pathwayDataPath); 

		String[] children = dir.list();
		if (children == null) {
			System.out.println("Cannot Load Network files from data path :"+pathwayDataPath);
			System.exit(0);
		}
		saveToFile sf = new saveToFile(DEBUGmode);
		Vector<String> outputFiles = new Vector();
		
		summary = summary.concat(children.length +" pathways examined.\n");
		
	    for (int i=0; i<children.length; i++) {
	        // Get filename of file or directory
	        String pathway = children[i];    
			File ftmp = new File(pathwayDataPath + pathway);
			if (ftmp.isDirectory())				//Ignore all the folders.
				continue;

			DecomposeNetwork dn = new DecomposeNetwork(DEBUGmode, ignoreInverseInhibition, inf, pathwayDataPath, pathway, ftmp.getAbsolutePath());
			if(dn.listNode == null)
				continue;
			
			FilterMAfromGRNgeneList f = new FilterMAfromGRNgeneList(DEBUGmode);
			Hashtable MAdata = f.filter(discr.genes, discrData, dn.listNode);

			inf.setNumberOfGenes(MAdata.size()-1);		
						
			dn.enrichSubpathsWithDublicateGenesAsOR(f.groupDublicateNames(discr.genes));
			
			Hashtable pathAndMA = dn.pathsWithMAvalues(MAdata);		
			if(pathAndMA != null){
				sf.addDataForMerge(pathAndMA, dn.nonFunctionalSubPaths);
				outputFiles.add(dataPath +"results/perPathway/" + MAdataFile +"." +pathway +".txt");
				inf.addNumOfpaths(pathAndMA.size()+dn.nonFunctionalSubPaths.size());
			}
			//Thread safe policy.

			f = null;
			dn.destroy();
			dn = null;
		    MAdata = null;
			pathAndMA = null;
	    }
	    if(outputFiles.size() >0){
	    	sf.mergeData(dataPath +"results/" + MAdataFile+"-all-pathways.txt", discr.classes);

			PolarityFilter pf;
			pf = new PolarityFilter(DEBUGmode, sf, discr.classes, false, inf, usePval, useFoldChange);
			pf.polarityMetric(dataPath +"results/" + MAdataFile+"-all-pathways.txt", _orange, threshold);
			
			
			sf.tabFile2arff( dataPath +"results/" + MAdataFile+"-Best.txt");
			
			inf.exportJsons();
			inf.setPvaluesPerPathway( sf.mergedData.size()+sf.mergedNonFunctionalSubPaths.size() );
			inf.exportPathwayStats();
			//inf.printAll();
			pf = null;
	    }
	    
	    if( (classifierId < 4) && (classifierId > 0) ){
			validation v = new validation();
			try {
				v.useValidation(dataPath +"results/" + MAdataFile+"-Best.txt.arff" , classifierId, dataPath +"results/validation.txt");
				summary = summary.concat(v.getValidationResults());
				System.out.println(v.getValidationResults());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    
	    discr = null;
		outputFiles = null;
		sf = null;
		inf = null;
	}
}