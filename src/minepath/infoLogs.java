package minepath;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import misc.FisherExact;

public class infoLogs implements Serializable{
	public String MAfileName = new String();
	public String dataPath;

	public String class1Name;
	public double class1Mean;
	public double class1Threshold;
	public double class1Std;
	public double class1min = 100;
	public double class1max = 0;	
	
	public String class2Name;
	public double class2Mean;
	public double class2Threshold;
	public double class2Std;
	public double class2min = -100;
	public double class2max = 0;	
	
	public double CommonThreshold;
	
	public List<pathInfo> paths = new ArrayList();
	
	public List subPathsPlus = new ArrayList();
	public List subPathsMinus = new ArrayList();
	public List subPathsCommon = new ArrayList();

	int totalInteractions = 0;
	int totalFunctionalInteractions = 0;
	public pathInfo cur;
	private Hashtable keggIDandTitle = new Hashtable();

	
	public infoLogs(String _dataPath, String keggTitles) {
		dataPath = _dataPath;
		if(keggTitles != null)
			readKeggTitles(keggTitles);
	}

	public void addPathInfo(String name, int genesInPathway, int interactionsInPathway){
		//pathInfo p = new pathInfo(totalSubPaths, pathwaysFisher);
		pathInfo p = new pathInfo();
		p.name = name;
		p.title = (String) keggIDandTitle.get(name.replaceAll(".xgmml", ""));
		p.numOfGenes = genesInPathway;
		p.numOfInteractionsInPathway = interactionsInPathway;
		totalInteractions = totalInteractions+interactionsInPathway;
		this.paths.add(p);
		cur = p;
	}
	
	public void setNumberOfGenes(int genes){
		cur.numOfGenes = genes;
	}
	
	private void readKeggTitles(String inputFileName){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(inputFileName));
			String line;
			String[] tmp = new String[2];
			while ( (line = reader.readLine()) != null) {	
				tmp = line.split("\t");
				keggIDandTitle.put(tmp[0], tmp[1]);
			}
			reader.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void addNumOfpaths(int subPaths){
		cur.numOfSubPaths = subPaths;
	}
	
	public void addPathEdgesWithScore(String pathName, double score, double pvalue, String coverage, boolean isBothUp){
		//if(Math.abs(score) <0.05 && !isBothUp)
		//	return;
		
		pathWithScore ps = new pathWithScore();
		ps.path = revertToOriginalPath(pathName);
		ps.coverage = coverage;
//if( isBothUp)
//	System.out.println(pathName +"\tInverted to:\t"+ps.path +"\t" +cur.name);		
		ps.score = score;
		ps.pvalue = pvalue;
		double scoringData;
		
		if (pvalue == Double.NaN)
			scoringData = pvalue;
		else
			scoringData = score;
		
		if(isBothUp){
			subPathsCommon.add(ps);
		}else if(scoringData>0){
			subPathsPlus.add(ps);
			if(scoringData > class1max)
				class1max = scoringData;
			if(scoringData < class1min)
				class1min = scoringData;
		}else{
			subPathsMinus.add(ps);
			if(scoringData < class2max)
				class2max = scoringData;
			if(scoringData > class2min)
				class2min = scoringData;
		}
	}
	
	
	private String revertToOriginalPath(String path){		
		String ret = path.split("@")[0];
		String[] tmp = ret.split("--");
		ret = "";
		for(int i=0; i<tmp.length; i++){
			if(tmp[i].startsWith(">")){
				ret = ret.concat("-->");
				tmp[i] = tmp[i].substring(1, tmp[i].length());
			}else if(tmp[i].startsWith("|")){
				ret = ret.concat("--|");
				tmp[i] = tmp[i].substring(1, tmp[i].length());
			}
			String[] wspaces = tmp[i].split(" ");
			String prevProbe = new String();
			for(int j=0; j <wspaces.length; j++){
				if(prevProbe.compareTo( wspaces[j].substring(wspaces[j].indexOf("#")+1) ) != 0){					
					prevProbe = wspaces[j].substring(wspaces[j].indexOf("#")+1);
					ret = ret.concat(prevProbe);
					ret = ret.concat(" ");		
				}
			}
			ret = ret.substring(0, ret.length()-1);
		}
		return ret;
	}
	
	public void add2path2edges(String path, String edges){
		//System.out.println("To Add path:" +path +"\tedges:" +edges);
		//System.out.println(cur.name);
		edges = "\"" +edges +"\"";
		cur.path2edge.put(path, edges);
//if(cur.name.startsWith("hsa04010") && path.startsWith("hsa:1432"))
//	System.out.println(cur.name +"\t" +path +"\t"+edges);		

	}

	public Hashtable getPath2edge(){
		return cur.path2edge;
	}
	
	public void setPath2edge(Hashtable p2e){
		cur.path2edge = p2e;
	}

	public void exportJsons() throws IOException {
		Collections.sort(subPathsPlus);
		Collections.sort(subPathsMinus);
		Collections.sort(subPathsCommon);
		//printLists();
		
		String generalJson = new String("\t\"MicroArray\" : \"" +this.MAfileName +"\",");
		generalJson = generalJson.concat("\n\t\"class1\" : \"" +this.class1Name +"\",");
		generalJson = generalJson.concat("\n\t\"class1mean\" : \"" +this.class1Mean +"\",");
		generalJson = generalJson.concat("\n\t\"class1std\" : \"" +this.class1Std +"\",");
		generalJson = generalJson.concat("\n\t\"class1thr\" : \"" +this.class1Threshold +"\",");
		generalJson = generalJson.concat("\n\t\"class1min\" : \"" +this.class1min +"\",");
		generalJson = generalJson.concat("\n\t\"class1max\" : \"" +this.class1max +"\",");
		generalJson = generalJson.concat("\n\t\"class2\" : \"" +this.class2Name +"\",");
		generalJson = generalJson.concat("\n\t\"class2mean\" : \"" +this.class2Mean +"\",");
		generalJson = generalJson.concat("\n\t\"class2std\" : \"" +this.class2Std +"\",");
		generalJson = generalJson.concat("\n\t\"class2thr\" : \"" +this.class2Threshold +"\",");
		generalJson = generalJson.concat("\n\t\"class2min\" : \"" +this.class2min +"\",");
		generalJson = generalJson.concat("\n\t\"class2max\" : \"" +this.class2max +"\",");
		generalJson = generalJson.concat("\n\t\"bothUpThr\" : \"" +this.CommonThreshold +"\",");
		
		
		for(int i=0; i<this.paths.size(); i++){
			pathInfo p = (pathInfo) this.paths.get(i);
			p.class1Threshold = this.class1Threshold;
			p.class2Threshold = this.class2Threshold;
			String json = new String("{\n");
			json = json.concat(generalJson);
			//json = json.concat("\n\t\"p-value\" : \"" +p.pvalue +"\","); //P-value hasn't been computed yet.
			
			json = json.concat("\n\t\"subPaths\":{");
			json = json.concat("\n\t\t\"Class1\":[");
			json = json.concat(p.exportJsonList(subPathsPlus, "Class1"));
			
			json = json.concat("\n\t\t],\n\t\t\"Class2\":[");
			json = json.concat(p.exportJsonList(subPathsMinus, "Class2"));
//System.out.println("Exporting Common");
			json = json.concat("\n\t\t],\n\t\t\"Common\":[");
			json = json.concat(p.exportJsonList(subPathsCommon, "Common"));
			json = json.concat("\n\t\t]\n\t}");
			json = json.concat("\n}");
			//System.out.println(json);
			
			totalFunctionalInteractions = totalFunctionalInteractions + p.activeInteractions.size();
			
			BufferedWriter writer = new BufferedWriter( new FileWriter(dataPath+"results/perPathway/" +this.MAfileName +"." +p.name+".json") );
			writer.write(json);
			writer.flush();
			writer.close();
		}
	}	
	
	public void exportPathwayStats() throws IOException{
		BufferedWriter writer = new BufferedWriter( new FileWriter(dataPath+"results/pathwayStats.xml") );
		String pathStats = new String("<MinePath>");
		pathStats = pathStats.concat("\n\t<Experiment_information>");
		pathStats = pathStats.concat("\n\t\t<DataSet>" +this.MAfileName +"</DataSet>");
		pathStats = pathStats.concat("\n\t\t<class1>" +this.class1Name +"</class1>");
		pathStats = pathStats.concat("\n\t\t<class1Mean>" +this.class1Mean +"</class1Mean>");
		pathStats = pathStats.concat("\n\t\t<class1Std>" +this.class1Std +"</class1Std>");
		pathStats = pathStats.concat("\n\t\t<class1Threshold>" +this.class1Threshold +"</class1Threshold>");
		pathStats = pathStats.concat("\n\t\t<class1max>" +this.class1max +"</class1max>");
		pathStats = pathStats.concat("\n\t\t<class2>" +this.class2Name +"</class2>");
		pathStats = pathStats.concat("\n\t\t<class2Mean>" +this.class2Mean +"</class2Mean>");
		pathStats = pathStats.concat("\n\t\t<class2Std>" +this.class2Std +"</class2Std>");
		pathStats = pathStats.concat("\n\t\t<class2Threshold>" +this.class2Threshold +"</class2Threshold>");
		pathStats = pathStats.concat("\n\t\t<class2min>" +this.class2min +"</class2min>");
		pathStats = pathStats.concat("\n\t\t<class2max>" +this.class2max +"</class2max>");
		pathStats = pathStats.concat("\n\t\t<commonThreshold>" +this.CommonThreshold +"</commonThreshold>");
		pathStats = pathStats.concat("\n\t</Experiment_information>");
		
		for(int i=0; i<this.paths.size(); i++){
			pathInfo p = (pathInfo) this.paths.get(i);
			p.class1Threshold = this.class1Threshold;
			p.class2Threshold = this.class2Threshold;
			pathStats = pathStats.concat(p.exportPathInfoXML());
		}
		pathStats = pathStats.concat("\n</MinePath>");

		writer.write(pathStats);
		writer.flush();
		writer.close();
	}
	
	public void printAll() {
		System.out.println("\n\n---------------------\nGeneral Information.\n");
		System.out.println("---------------------");
		System.out.println("Microarray File name\t:" +this.MAfileName);
		System.out.println("\nClass 1\nName:\t\t" +this.class1Name);
		System.out.println("Mean:\t\t" +String.format(Locale.US, "%.20f", this.class1Mean));
		System.out.println("StD:\t\t" +String.format(Locale.US, "%.20f", this.class1Std));
		System.out.println("Threshold:\t" +String.format(Locale.US, "%.20f", this.class1Threshold));
		System.out.println("\nClass 2\nName:\t\t" + this.class2Name);
		System.out.println("Mean:\t\t" +String.format(Locale.US, "%.20f", this.class2Mean));
		System.out.println("StD:\t\t" +String.format(Locale.US, "%.20f", this.class2Std));
		System.out.println("Threshold:\t" +String.format(Locale.US, "%.20f", this.class2Threshold));
		System.out.println("\nThreshold for functional sub-paths at both classes :"+this.CommonThreshold);		
		
		for(int i=0; i<this.paths.size(); i++){
			pathInfo p = (pathInfo) this.paths.get(i);
			/*
			String tmpPath;
			//Class1
			for(int j=0; j<subPathsPlus.size(); j++){
				tmpPath = ( (pathWithScore) subPathsPlus.get(i)).path;
			}
			*/
			p.printAll();
		}
	}

	public void printLists(){
		pathWithScore tmp;
		System.out.println("\nSub-paths for Class 1.");
		for(int i=0; i<subPathsPlus.size(); i++)
			( (pathWithScore) subPathsPlus.get(i)).printIt();
		System.out.println("\nSub-paths for Class 2.");
		for(int i=0; i<subPathsMinus.size(); i++)
			( (pathWithScore) subPathsMinus.get(i)).printIt();
		System.out.println("\nSub-paths for Both Classes.");
		for(int i=0; i<subPathsCommon.size(); i++)
			( (pathWithScore) subPathsCommon.get(i)).printIt();
		System.out.println("\n");
	}

	public void setPvaluesPerPathway(int totalSubPaths) {

		FisherExact pathwaysFisher = new FisherExact(totalSubPaths +this.subPathsMinus.size() + this.subPathsPlus.size() +1);	
		
		for(int i=0; i<this.paths.size(); i++){
			pathInfo p = (pathInfo) this.paths.get(i);
			int subInPathway = p.numOfSubPathsClass1 +p.numOfSubPathsClass2;
			int bestSubPathsNotInPathway = this.subPathsMinus.size() + this.subPathsPlus.size() - subInPathway;
			if(subInPathway > 1) //More conservative than Fisher exact. Used by DAVID.
				subInPathway = subInPathway -1;

			//P-value per sub-paths
			double oldPval = pathwaysFisher.getRightTailedP(subInPathway, p.numOfSubPaths, bestSubPathsNotInPathway, (totalSubPaths- p.numOfSubPaths));
			p.pvalue = 1;
			if(p.activeInteractions.size() >0)
				p.pvalue = pathwaysFisher.getRightTailedP(p.activeInteractions.size()-1, p.numOfInteractionsInPathway, (this.totalFunctionalInteractions-p.activeInteractions.size()), (this.totalInteractions -p.numOfInteractionsInPathway) );
			else
				p.pvalue = 1;
			if(p.pvalue > 0.99)
				p.pvalue = 1;			

		}
		
	}
	

}



class pathInfo implements Serializable{
	public String name;
	public String title;
	int numOfGenes;
	int numOfInteractionsInPathway;
	int numOfSubPaths;
	Hashtable activeInteractions = new Hashtable();
	double class2Threshold;
	double class1Threshold;
	int numOfSubPathsClass1 = 0;
	int numOfSubPathsClass2 = 0;
	int numOfSubPathsOverThrClass1 = 0;
	int numOfSubPathsOverThrClass2 = 0;
	int numOfSubPathsCommon = 0;
	double pvalue;
	
	public Hashtable path2edge = new Hashtable();
/*
	pathInfo(int totalSubs, FisherExact _pathwaysFisher){
		totalSubPaths = totalSubs;
		//pathwaysFisher = _pathwaysFisher;
	}
*/

	public Hashtable getPath2edge(){
		return path2edge;
	}
	
	public void setPath2edge(Hashtable p2e){
		path2edge = p2e;
	}
	
	public String exportJsonList(List theList, String className){
		String ret = new String("");
		for(int i=0; i<theList.size(); i++){
			pathWithScore ps = (pathWithScore) theList.get(i);
			String edges = (String) path2edge.get(ps.path);
			if(edges == null)
				continue;
			
			if(className.compareTo("Class1") == 0){
				numOfSubPathsClass1++;
				if(ps.score >= this.class1Threshold)
					numOfSubPathsOverThrClass1++;
			}else if(className.compareTo("Class2") == 0){
				numOfSubPathsClass2++;
				if(ps.score <= this.class2Threshold)
					numOfSubPathsOverThrClass2++;
			}else if(className.compareTo("Common") == 0)
				numOfSubPathsCommon++;
			if(className.compareTo("Common") != 0){
				String[] tmpEdges = edges.substring(1, edges.length()-1).split(",");
				for(int j=0; j<tmpEdges.length; j++)
					activeInteractions.put(tmpEdges[j], tmpEdges[j]);
			}
			ret = ret.concat("\n\t\t\t{ \"score\":" +ps.score +",\"pvalue\":" +Math.abs(ps.pvalue)+",\"coverage\":\"" +ps.coverage +"\", \"EdgesIndex\":[" +edges.substring(1, edges.length()-2) +"]},");
		}

		if(ret.length() > 1)
			ret = ret.substring(0, ret.length()-1); //remove last comma

		return ret;
	}
	
	public void printAll(){
	
		System.out.println("\n--------------\nPathway Information.\n");
		System.out.println("Name:\t\t\t" +this.name);
		System.out.println("Num of Genes:\t\t" +this.numOfGenes);
		System.out.println("Num of Subpaths:\t" +this.numOfSubPaths);
		System.out.println("Threshold for Class 1:\t" +this.class1Threshold);
		System.out.println("Threshold for Class 2:\t" +this.class2Threshold);
		System.out.println("Num of Subpaths in class 1:\t" +this.numOfSubPathsClass1);
		System.out.println("Num of Subpaths in class 1 over Threshold:\t" +this.numOfSubPathsOverThrClass1);
		System.out.println("Num of Subpaths in class 2:\t" +this.numOfSubPathsClass2);
		System.out.println("Num of Subpaths in class 2 over Threshold:\t" +this.numOfSubPathsOverThrClass2);
		System.out.println("Num of Subpaths in common :\t" +this.numOfSubPathsCommon);

		System.out.println();
		/*
		Set set = this.path2edge.entrySet(); 
		Iterator it = set.iterator(); 
		while(it.hasNext()) { 
			Map.Entry me = (Map.Entry)it.next(); 
			System.out.println( (String)me.getKey() +"\tEdges :" +(String)me.getValue() );	
		}
		*/
	}
	
	public String exportPathInfoXML(){
		String ret = new String("\n\t<Pathway>");
		ret = ret.concat("\n\t\t<name>" +this.name +"</name>");
		ret = ret.concat("\n\t\t<title>" +this.title +"</title>");
		ret = ret.concat("\n\t\t<numOfGenes>" +this.numOfGenes +"</numOfGenes>");
		ret = ret.concat("\n\t\t<numOfSubPaths>" +this.numOfSubPaths +"</numOfSubPaths>");
		ret = ret.concat("\n\t\t<numOfInteractions>" +this.numOfInteractionsInPathway +"</numOfInteractions>");
		ret = ret.concat("\n\t\t<activeInteractions>" +this.activeInteractions.size() +"</activeInteractions>");
		
		double tmpSubPathsFound	= (double)(this.numOfSubPathsOverThrClass1 + this.numOfSubPathsOverThrClass2 + this.numOfSubPathsCommon);
		double pwA 				= (this.numOfSubPaths==0)? 0 : tmpSubPathsFound/this.numOfSubPaths;
		double pwDS 			= (tmpSubPathsFound==0)  ? 0 : (double)(this.numOfSubPathsOverThrClass1 + this.numOfSubPathsOverThrClass2)/tmpSubPathsFound;
		double score 			= pwA + (1-pvalue);
		DecimalFormat df = new DecimalFormat("#.##");
		ret = ret.concat("\n\t\t<score>" +df.format(score) +"</score>");
		if(pvalue > 0.01)
			ret = ret.concat("\n\t\t<p-value>" + String.format("%.4f", pvalue)  +"</p-value>");
		else
			ret = ret.concat("\n\t\t<p-value>" + String.format("%.20f", pvalue)  +"</p-value>");
		ret = ret.concat("\n\t\t<pwA>" +(int)tmpSubPathsFound +"</pwA>");
		ret = ret.concat("\n\t\t<pwDS>" +df.format(pwDS) +"</pwDS>");
		
		ret = ret.concat("\n\t\t<numOfSubPathsClass1>" +this.numOfSubPathsClass1 +"</numOfSubPathsClass1>");
		ret = ret.concat("\n\t\t<numOfSubPathsOverThrClass1>" +this.numOfSubPathsOverThrClass1 +"</numOfSubPathsOverThrClass1>");
		ret = ret.concat("\n\t\t<persOfSubPathsOverThrClass1>" +(int)( ((double)this.numOfSubPathsOverThrClass1/this.numOfSubPaths)*100) +"</persOfSubPathsOverThrClass1>");
		ret = ret.concat("\n\t\t<numOfSubPathsClass2>" +this.numOfSubPathsClass2 +"</numOfSubPathsClass2>");
		ret = ret.concat("\n\t\t<numOfSubPathsOverThrClass2>" +this.numOfSubPathsOverThrClass2 +"</numOfSubPathsOverThrClass2>");
		ret = ret.concat("\n\t\t<persOfSubPathsOverThrClass2>" +(int)( ((double)this.numOfSubPathsOverThrClass2/this.numOfSubPaths)*100) +"</persOfSubPathsOverThrClass2>");
		ret = ret.concat("\n\t\t<numOfSubPathsCommon>" +this.numOfSubPathsCommon +"</numOfSubPathsCommon>");
		
		ret = ret.concat("\n\t</Pathway>");
		return ret;
	}
}


class pathWithScore implements Comparable{
	String path;
	double score;
	double pvalue;
	String coverage;
	
    public int compare(Object _ps1, Object _ps2) {
    	pathWithScore ps1 = (pathWithScore) _ps1;
    	pathWithScore ps2 = (pathWithScore) _ps2;
        return (ps1.score < ps2.score ) ? 1: (ps1.score > ps2.score) ? -1:0 ;
    }

	public int compareTo(Object rhs) {
		return compare(this, rhs);
	}
	
	public void printIt(){
		System.out.println(score +"\t"+path);
	}

}


