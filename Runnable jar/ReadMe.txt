Running MinePath

MinePath <MicroArray FileName full path> <Pathways Folder Path> 
	Optional (parameters must appear after microarray & pathways paths):
		-Debug (start MinePath in debug mode. Creates files per pathway and other intermediate files)
		-cloneSubPaths generate as new subpaths all the possible combinations of multi-probes per gene and gene groups in KEGG nodes
			 Warning this is computational intensive. Please use Xmx6000m as parameter and don't use it in conjuction with -Debug option

		-kmeans set K-means as discretization method (default Entropy based)
		-mean   set Mean    as discretization method (default Entropy based)
		-upBoth N (% selected up regulated sub-paths for both classes) 	Default 90
		-addSD N (Add SD at the Threshold. Positive value (e.g. 0.5 or 1 or 2) makes threshold more strict. Negative value makes threshold more elastic) 	Default 0
		-addStartEnd (add as subpath the first - last genes of every big (over 2 reactions) sub-path) 		Default false 
		-i (ignore Paths with only 1 reaction) 		Default false 
		-ignoreInverseInhibition (ignore inverse inhibition Down --| Up) Default false (use it)
		-filterSimpleRelative (R1-R2)/(R1+R2) 					Default = Polarity filter with relative values
		-filterAbsolute (use absolute values for filtering ) 	Default = Polarity filter with relative values
		#-classifier (10 fold cross validation of best subpaths)	1 = C4.5 decision tree , 2 NaiveBayes, 3 Support Vector Machines (Default), 0 none

Examples:
To run MinePath in standard mode with entropy based disctretization 
java -jar MinePath.jar datasets/GSE2034.txt pathways/all/ 

To run MinePath in standard mode with K-Means disctretization 
java -jar MinePath.jar datasets/GSE2034.txt pathways/all/ -kmeans

To run MinePath in Debug mode with K-Means disctretization 
java -jar MinePath.jar datasets/GSE2034.txt pathways/all/ -Debug -kmeans


